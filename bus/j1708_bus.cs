﻿/*!
 * @file j1708_bus.cs
 * @project Adv+ AVL Test Application
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @note Compiled with Visual Studio 2015
 * @copyright 2019 Muncie Power Products
 * @section DESCRIPTION
 * This file contains a can-bus interface class designed to work with SysTec's
 * USB-CANmodul
 * 
 * @section LICENSE
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * 1) Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 *   
 * 2) Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.
 *   
 * 3) Neither the name of the copyright holder nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Win32;


namespace ADV_CAN_Datalogging
{
    class j1708_bus
    {
        private SerialPort serial;              /*!< Serial port */

        //events
        private Action read_serial_data = null;
        private byte[] io_buffer;               /*!< RX Buffer */
        private byte[] read_buffer;
        
        int io_ptr;
        int bytesRead;

        public delegate void ErrorHandler(object sender, string e);
        public event ErrorHandler OnErrorPosted;
        public delegate void MessageHandler(object sender, J1708_PACKET data);
        public event MessageHandler OnMessageRecieved;


        public j1708_bus()
        {
            serial = new SerialPort();
            io_buffer = new byte[100];
            read_buffer = new byte[20];
            io_ptr = 0;

            read_serial_data = delegate
            {
                if (serial.IsOpen)
                {
                    serial.BaseStream.BeginRead(read_buffer, 0, read_buffer.Length, delegate (IAsyncResult ar)
                    {
                        try
                        {
                            bytesRead = serial.BaseStream.EndRead(ar);
                            Buffer.BlockCopy(read_buffer, 0, io_buffer, io_ptr, bytesRead);
                            io_ptr += bytesRead;

                            for(int i=0; i < io_ptr; i++)
                            {
                                if (i > 0 && io_buffer[i] == 200)
                                {
                                    J1708_PACKET pkt = new J1708_PACKET();
                                    if (i > 3)
                                    {
                                        pkt.id = (byte)io_buffer[0];
                                        pkt.len = (byte)(i - 2);
                                        pkt.checksum = (byte)io_buffer[i - 1];
                                        for (uint j = 1; j < i - 1 && j < 20; j++)
                                        {
                                            pkt.data[j-1] = io_buffer[j];
                                        }
                                        OnMessageRecieved?.Invoke(this, pkt);
                                    }


                                    io_ptr = 1;
                                    break;
                                }

                            }
                            if (io_ptr > 60)
                            {
                                io_ptr = 0;
                            }
                        }
                        catch (Exception)
                        {
                            return;
                        }
                        read_serial_data();
                    }, null);
                }
            };
        }

        public bool IsRunning()
        {
            return serial.IsOpen;
        }


        public bool Initialize(string port)
        {
            if (serial.IsOpen)
            {
                return true;
            }
            serial.PortName = port;
            serial.BaudRate = 9600;
            serial.DataBits = 8;
            serial.Parity = Parity.None;
            serial.StopBits = StopBits.One;
        
            try
            {
                serial.Open();
                read_serial_data();
            }
            catch (Exception)
            {
                PostError("Could not open UART Port");
                return false;
            }
            return true;
        }

        public void Close()
        {
            serial.Close();
        }


        public string FindPort(string vid, string pid)
        {
            List<string> dev_ports = getComportsByDevId(vid, pid);
            string[] active_ports = SerialPort.GetPortNames();
            foreach (string t in active_ports)
            {
                if (dev_ports.Contains(t))
                {
                    return t;
                }
            }
            return "";
        }

        private List<string> getComportsByDevId(string vid, string pid)
        {
            List<string> comports = new List<string>();

            if (IsLinux())
            {
                string[] temp = Directory.GetFiles("/dev/", "ttyUSB*");
                comports = new List<string>(temp);
            }
            else
            {
                String pattern = String.Format("^VID_{0}.PID_{1}", vid, pid);
                Regex _rx = new Regex(pattern, RegexOptions.IgnoreCase);
                RegistryKey rk1 = Registry.LocalMachine;
                RegistryKey rk2 = rk1.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum");
                foreach (String s3 in rk2.GetSubKeyNames())
                {
                    RegistryKey rk3 = rk2.OpenSubKey(s3);
                    foreach (String s in rk3.GetSubKeyNames())
                    {
                        if (_rx.Match(s).Success)
                        {
                            RegistryKey rk4 = rk3.OpenSubKey(s);
                            foreach (String s2 in rk4.GetSubKeyNames())
                            {
                                RegistryKey rk5 = rk4.OpenSubKey(s2);
                                if (rk5.OpenSubKey("Device Parameters") != null)
                                {
                                    RegistryKey rk6 = rk5.OpenSubKey("Device Parameters");
                                    comports.Add((string)rk6.GetValue("PortName"));
                                }
                            }
                        }
                    }
                }
            }

            return comports;
        }

        private bool IsLinux()
        {
            int p = (int)Environment.OSVersion.Platform;
            return (p == 4) || (p == 6) || (p == 128);
        }

        private void PostError(string s)
        {
            OnErrorPosted?.Invoke(this, s);
        }
    }

    public class J1708_PACKET
    {
        public byte id;
        public byte len;
        public byte checksum;
        public int[] data = new int[20];
    }
}
