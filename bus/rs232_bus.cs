﻿/*!
 * @file rs232_bus.cs
 * @project Adv+ AVL Test Application
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @note Compiled with Visual Studio 2015
 * @copyright 2019 Muncie Power Products
 * @section DESCRIPTION
 * This file contains a can-bus interface class designed to work with SysTec's
 * USB-CANmodul
 * 
 * @section LICENSE
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * 1) Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 *   
 * 2) Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.
 *   
 * 3) Neither the name of the copyright holder nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.IO;
using System.Text.RegularExpressions;
using Microsoft.Win32;

namespace ADV_CAN_Datalogging
{
    class rs232_bus
    {
        private SerialPort serial;              /*!< Serial port */

        //events
        private string io_buffer;
        private uint rxStage;
        RS232_PACKET packet = new RS232_PACKET();

        public delegate void ErrorHandler(object sender, string e);
        public event ErrorHandler OnErrorPosted;
        public delegate void MessageHandler(object sender, RS232_PACKET data);
        public event MessageHandler OnMessageRecieved;

        public rs232_bus()
        {
            serial = new SerialPort();
            rxStage = 0;
            

            serial.DataReceived += Serial_DataReceived;
        }

        private void Serial_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                string temp = serial.ReadExisting();
                io_buffer += temp;
                if (io_buffer[io_buffer.Length - 1] == '\n')
                {
                    for (int i = 0; i < io_buffer.Length; i++)
                    {
                        if (rxStage == 0 && i > 2 && io_buffer[i] == '|')
                        {
                            packet.id = "";
                            packet.id += (char)io_buffer[i - 3];
                            packet.id += (char)io_buffer[i - 2];
                            packet.id += (char)io_buffer[i - 1];
                            packet.len = 0;
                            packet.data = "";
                            packet.checksum = 0;
                            rxStage = 1;
                        }
                        else if (rxStage == 1 && i > 3 && io_buffer[i] == '|')
                        {
                            temp = "";
                            temp += (char)io_buffer[i - 3];
                            temp += (char)io_buffer[i - 2];
                            temp += (char)io_buffer[i - 1];
                            packet.len = Convert.ToInt32(temp, 16);
                            rxStage = 2;
                        }
                        else if (rxStage == 2)
                        {
                            if (io_buffer[i] == '|')
                            {
                                rxStage = 3;
                            }
                            else
                            {
                                packet.data += (char)io_buffer[i];
                            }
                        }
                        else if (rxStage == 3 && io_buffer[i] == '\r')
                        {
                            temp = "";
                            temp += (char)io_buffer[i - 4];
                            temp += (char)io_buffer[i - 3];
                            temp += (char)io_buffer[i - 2];
                            temp += (char)io_buffer[i - 1];
                            packet.checksum = Convert.ToInt32(temp, 16);
                            int crc = crc16_calculate(packet.data, packet.len);
                            packet.checksumValid = (crc == packet.checksum);
                            rxStage = 0;
                            OnMessageRecieved?.Invoke(this, packet);
                        }

                    }
                    io_buffer = ""; ;
                }

                if (io_buffer.Length > 200)
                {
                    io_buffer = "";
                }

            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public bool IsRunning()
        {
            return serial.IsOpen;
        }

        public bool Initialize(string port, int baud)
        {
            if (serial.IsOpen)
            {
                Close();
            }
            serial.PortName = port;
            serial.BaudRate = baud;
            serial.DataBits = 8;
            serial.Parity = Parity.Even;
            serial.StopBits = StopBits.One;

            try
            {
                serial.Open();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                PostError("Could not open UART Port");
                return false;
            }
            return true;
        }

        public void Close()
        {
            serial.DiscardInBuffer();
            serial.DiscardOutBuffer();
            serial.Close();
        }

        public void Send(RS232_PACKET data)
        {
            char[] tx_buff = new char[data.len + 15];
            string temp;
            int i = 0, crc;

            tx_buff[0] = data.id[0];
            tx_buff[1] = data.id[1];
            tx_buff[2] = data.id[2];
            tx_buff[3] = '|';
            temp = str_toHexBuffer(data.len, 3);
            tx_buff[4] = temp[0];
            tx_buff[5] = temp[1];
            tx_buff[6] = temp[2];
            tx_buff[7] = '|';
            for(i = 0; i < data.len; i++)
            {
                tx_buff[i + 8] = data.data[i];
            }
            i += 8;
            crc = crc16_calculate(data.data,data.len);
            temp = str_toHexBuffer(crc, 4);
            tx_buff[i++] = '|';
            tx_buff[i++] = temp[0];
            tx_buff[i++] = temp[1];
            tx_buff[i++] = temp[2];
            tx_buff[i++] = temp[3];
            tx_buff[i++] = '\r';
            tx_buff[i++] = '\n';

            serial.Write(tx_buff, 0, data.len + 15);
        }

        private string str_toHexBuffer(int conv, int len)
        {
            int getter = 0xF, val, offset = 0;
            string ret = "";

            for (uint i = 0; i < len; i++)
            {
                val = (conv & getter) >> offset;
                ret = val.ToString("X") + ret;
                getter <<= 4;
                offset += 4;
            }
            return ret;
        }

        private int crc16_calculate(string data, int len)
        {
            ushort crc = 0xFFFF, val;

            for(int i = 0; i < len; i++)
            {
                val = (ushort)data[i];
                val <<= 8;
                crc ^= val;
                for(int j = 0; j < 8; j++)
                {
                    if ((crc & 0x8000) != 0)
                    {
                        crc <<= 1;
                        crc ^= 0x1021;
                    }
                    else
                    {
                        crc <<= 1;
                    }
                }
            }
            return (int)crc;
        }

        public string FindPort(string vid, string pid)
        {
            List<string> dev_ports = getComportsByDevId(vid, pid);
            string[] active_ports = SerialPort.GetPortNames();
            foreach (string t in active_ports)
            {
                if (dev_ports.Contains(t))
                {
                    return t;
                }
            }
            return "";
        }

        private List<string> getComportsByDevId(string vid, string pid)
        {
            List<string> comports = new List<string>();

            if (IsLinux())
            {
                string[] temp = Directory.GetFiles("/dev/", "ttyUSB*");
                comports = new List<string>(temp);
            }
            else
            {
                String pattern = String.Format("^VID_{0}.PID_{1}", vid, pid);
                Regex _rx = new Regex(pattern, RegexOptions.IgnoreCase);
                RegistryKey rk1 = Registry.LocalMachine;
                RegistryKey rk2 = rk1.OpenSubKey("SYSTEM\\CurrentControlSet\\Enum");
                foreach (String s3 in rk2.GetSubKeyNames())
                {
                    RegistryKey rk3 = rk2.OpenSubKey(s3);
                    foreach (String s in rk3.GetSubKeyNames())
                    {
                        if (_rx.Match(s).Success)
                        {
                            RegistryKey rk4 = rk3.OpenSubKey(s);
                            foreach (String s2 in rk4.GetSubKeyNames())
                            {
                                RegistryKey rk5 = rk4.OpenSubKey(s2);
                                if (rk5.OpenSubKey("Device Parameters") != null)
                                {
                                    RegistryKey rk6 = rk5.OpenSubKey("Device Parameters");
                                    comports.Add((string)rk6.GetValue("PortName"));
                                }
                            }
                        }
                    }
                }
            }

            return comports;
        }

        private bool IsLinux()
        {
            int p = (int)Environment.OSVersion.Platform;
            return (p == 4) || (p == 6) || (p == 128);
        }

        private void PostError(string s)
        {
            OnErrorPosted?.Invoke(this, s);
        }

    }

    public class RS232_PACKET
    {
        public string id;
        public int len;
        public int checksum;
        public bool checksumValid;
        public string data;
    }
}
