﻿/*!
 * @file SysBus.cs
 * @project Adv+ AVL Test Application
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @note Compiled with Visual Studio 2015
 * @copyright 2019 Muncie Power Products
 * @section DESCRIPTION
 * This file contains the UI for the system
 * 
 * @section LICENSE
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * 1) Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 *   
 * 2) Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.
 *   
 * 3) Neither the name of the copyright holder nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE.
 */
using System;
using System.Threading;

namespace ADV_CAN_Datalogging
{
    class SysBus
    {
        private usb_can cbus;
        private j1708_bus jBus = new j1708_bus();
        private rs232_bus rBus = new rs232_bus();
        private SysBusDataRx rxData;
        private string[] temp_tid = new string[2];

        public SysBus()
        {
            rxData = new SysBusDataRx();
            cbus = new usb_can();
            cbus.OnMessageRecieved += CanServer_OnMessageRecieved;
            jBus.OnMessageRecieved += JBus_OnMessageRecieved;
            rBus.OnMessageRecieved += RBus_OnMessageRecieved;

        }

        public bool IsRunning()
        {
            return cbus.IsRunning() || jBus.IsRunning() || rBus.IsRunning();
        }

        public SysBusDataRx data()
        {
            return rxData;
        }

        public bool StartCan()
        {
            if (cbus.IsRunning())
            {
                return true;
            }
            return cbus.Start();
        }

        public bool StopCan()
        {
            if (!cbus.IsRunning())
            {
                return true;
            }
            return cbus.Stop();
        }

        public bool StartJ1708()
        {
            if (jBus.IsRunning())
            {
                return true;
            }
            string port = jBus.FindPort("0403", "6001");
            if (port.Length > 0)
            {
                return jBus.Initialize(port);
            }
            return false;
        }

        public bool StopJ1708()
        {
            if (!jBus.IsRunning())
            {
                return true;
            }
            jBus.Close();
            return true;
        }

        public bool StartRS232()
        {
            if (rBus.IsRunning())
            {
                return true;
            }
            string port = rBus.FindPort("0403", "6001");
            if (port.Length > 0)
            {
                if (rBus.Initialize(port, 9600))
                {
                    RS232_PACKET p = new RS232_PACKET();
                    p.id = "BDR";
                    p.data = "115200";
                    p.len = 6;
                    rBus.Send(p);
                    Thread.Sleep(200);
                    if (rBus.Initialize(port, 115200))
                    {
                        Thread.Sleep(200);
                        p.id = "INI";
                        p.data = "";
                        p.len = 0;
                        rBus.Send(p);
                        return true;
                    }
                    else
                    {
                        Console.WriteLine("Err 115200");
                    }
                }
                else
                {
                    Console.WriteLine("Err 9600");
                }
            }
            return false;
        }

        public bool StopRS232()
        {
            if (!rBus.IsRunning())
            {
                return true;
            }
            rBus.Close();
            return true;
        }

        public bool tx(uint type, SysCanDataTx data)
        {
            CAN_PACKET p = new CAN_PACKET();

            //initialization
            if (type == 0)
            {
                p.id = 0x011;
                p.len = 1;
                p.data[0] = 1;
                cbus.sendTx(p);

                p = new CAN_PACKET();
                p.id = 0x011;
                p.len = 1;
                p.data[0] = 2;
                cbus.sendTx(p);

                p = new CAN_PACKET();
                p.id = 0x102;
                p.len = 2;
                p.data[0] = 0x4E;
                p.data[1] = 5;
                cbus.sendTx(p);
            }
            //rmt control
            else if(type == 1)
            {
                p = new CAN_PACKET();
                p.id = 0x011;
                p.len = 4;
                p.data[0] = 50;
                p.data[1] = data.ctrlRate[0] & 0xFF;
                p.data[2] = data.ctrlRate[1] & 0xFF;
                p.data[3] = data.ctrlRate[2] & 0xFF;
                cbus.sendTx(p);
            }
            else if(type == 2)
            {

            }
            return true;
        }

        private void CanServer_OnMessageRecieved(object sender, EventArgs e)
        {
            PacketEventArgs args = (PacketEventArgs)e;
            int tempi;
            int[] data = args.packet.data;
            char[] buffer;

            switch (args.packet.id)
            {
                case 0x050:
                    rxData.packetCounter++;
                    rxData.ctrlVer = String.Format("{0}.{1}.{2}", data[0], data[1], data[2]);
                    rxData.uiVer = String.Format("{0}.{1}.{2}", data[3], data[4], data[5]);
                    rxData.lastCal = UnixToWindow((data[6] | data[7] << 8) * 24 * 60 * 60);
                    break;
                case 0x051:
                    rxData.packetCounter++;
                    rxData.serial = String.Format("{0:X}:{1:X}:{2:X}:{3:X}:{4:X}:{5:X}", data[0], data[1], data[2], data[3], data[4], data[5]);
                    break;
                case 0x052:
                    rxData.packetCounter++;
                    temp_tid[0] = new string(Array.ConvertAll(data, item => (char)item));
                    rxData.truckId = temp_tid[0] + temp_tid[1];
                    break;
                case 0x053:
                    rxData.packetCounter++;
                    temp_tid[1] = new string(Array.ConvertAll(data, item => (char)item));
                    rxData.truckId = temp_tid[0] + temp_tid[1];
                    break;
                case 0x060:
                    rxData.packetCounter++;
                    rxData.systemRuntime = data[0] | (data[1] << 8) | (data[2] << 16);
                    rxData.protocolVersion = (args.packet.len >= 4)?data[3]:0;
                    break;
                case 0x054:
                    rxData.packetCounter++;
                    rxData.groundspeed = data[0];
                    rxData.voltage = Convert.ToDouble(data[1]) / 10;
                    tempi = (int)data[2] | ((int)(data[4] & 0xF) << 8);
                    rxData.airTemp = (tempi == 0xFFF) ? 0xFFF : (Convert.ToDouble(tempi) * 0.1 - 60);
                    tempi = (int)data[3] | ((int)(data[4] & 0xF0) << 4);
                    rxData.roadTemp = (tempi == 0xFFF) ? 0xFFF : (Convert.ToDouble(tempi) * 0.1 - 60);
                    rxData.pumpP = data[5] * 25;
                    rxData.loadP = data[6] * 25;
                    rxData.auto = ((data[7] & 0x1) > 0) ? true : false;
                    rxData.augerCl = ((data[7] & 0x2) > 0) ? true : false;
                    rxData.prewetCl = ((data[7] & 0x4) > 0) ? true : false;
                    rxData.pause = ((data[7] & 0x8) > 0) ? true : false;
                    rxData.blast = ((data[7] & 0x10) > 0) ? true : false;
                    rxData.laneMode = ((data[7] & 0x20) > 0) ? true : false;
                    break;
                case 0x055:
                    rxData.packetCounter++;
                    rxData.spreaderMode = data[0];
                    rxData.augerRate = data[1] * (rxData.auto ? 50 : 1);
                    rxData.auger2Rate = data[2] * (rxData.auto ? 50 : 1);
                    if(rxData.spreaderMode == 3 || rxData.spreaderMode == 5)
                    {
                        rxData.spinnerRate = 0;
                        rxData.spinnerDir[0] = data[3] & 0xF;
                        rxData.spinnerDir[1] = (data[3] & 0xF0) >> 4;
                    }
                    else
                    {
                        rxData.spinnerRate = data[3];
                    }
                    rxData.preWetRate = data[4];
                    rxData.antiIceRate = data[5];
                    rxData.avgAuger = data[6] * 50;
                    rxData.avgPreWet = data[7];
                    break;
                case 0x056:
                    rxData.packetCounter++;
                    rxData.avgAntiIce = data[0];
                    if (rxData.protocolVersion < 6)
                    {
                        rxData.P2PDist = Convert.ToDouble(data[1] | (data[2] << 8)) * 0.0001;
                        rxData.P2PDistBlast = Convert.ToDouble(data[3] | (data[4] << 8)) * 0.0001;
                        rxData.P2PProdOutput = Convert.ToDouble(data[5] | (data[6] << 8) | (data[7] << 16)) * 0.0001;

                    }
                    else
                    {
                        rxData.totalDist = data[1] | (data[2] << 8) | (data[3] << 16);
                        rxData.totalDistSpread = data[4] | (data[5] << 8) | (data[6] << 16);
                    }
                    break;
                case 0x057:
                    rxData.packetCounter++;
                    if(rxData.protocolVersion < 6)
                    {
                        rxData.P2PProdBlastOutput = Convert.ToDouble(data[0] | (data[1] << 8) | (data[2] << 16)) * 0.0001; ;
                        rxData.P2PPreWetOutput = Convert.ToDouble(data[3] | (data[4] << 8) | (data[5] << 16)) * 0.0001;
                    }
                    else
                    {
                        rxData.totalDistBlast = data[0] | (data[1] << 8) | (data[2] << 16);
                        rxData.totalProdBlast = data[3] | (data[4] << 8) | (data[5] << 16);
                    }
                    rxData.preWetMode = data[6];
                    rxData.oilLevel = data[7];
                    break;
                case 0x058:
                    rxData.packetCounter++;
                    if(rxData.protocolVersion < 6)
                    {
                        rxData.P2PAntiIceOutput = Convert.ToDouble(data[0] | (data[1] << 8) | (data[2] << 16)) * 0.0001;
                    }
                    rxData.totalProd = data[3] | (data[4] << 8) | (data[5] << 16);
                    tempi = data[6] | (data[7] << 8);
                    rxData.oilTemp = (tempi == 0xFFF)?0xFFF: Convert.ToDouble(tempi) * 0.1 - 60;
                    break;
                case 0x059:
                    rxData.packetCounter++;
                    rxData.totalLiq = data[0] | (data[1] << 8) | (data[2] << 16);
                    buffer = Array.ConvertAll(data, item => (char)item);
                    rxData.prodName = new string(buffer, 3, 5);
                    break;
                case 0x5A:
                    rxData.packetCounter++;
                    buffer = Array.ConvertAll(data, item => (char)item);
                    rxData.route = new string(buffer, 0, 4);
                    rxData.driver = new string(buffer, 4, 4);
                    break;
                case 0x5B:
                    rxData.packetCounter++;
                    rxData.liqTankLevel = data[0];
                    rxData.bodyId = data[1];
                    rxData.dig[0] = ((data[2] & 0x1) > 0) ? true : false;
                    rxData.dig[1] = ((data[2] & 0x2) > 0) ? true : false;
                    rxData.dig[2] = ((data[2] & 0x4) > 0) ? true : false;
                    rxData.dig[3] = ((data[2] & 0x8) > 0) ? true : false;
                    rxData.dig[4] = ((data[2] & 0x10) > 0) ? true : false;
                    rxData.dig[5] = ((data[2] & 0x20) > 0) ? true : false;
                    rxData.dig[6] = ((data[2] & 0x40) > 0) ? true : false;
                    rxData.dig[7] = ((data[2] & 0x80) > 0) ? true : false;
                    rxData.dig[8] = ((data[3] & 0x1) > 0) ? true : false;
                    rxData.dig[9] = ((data[3] & 0x2) > 0) ? true : false;
                    rxData.dig[10] = ((data[3] & 0x4) > 0) ? true : false;
                    rxData.dig[11] = ((data[3] & 0x8) > 0) ? true : false;
                    rxData.dig[12] = ((data[3] & 0x10) > 0) ? true : false;
                    rxData.dig[13] = ((data[3] & 0x20) > 0) ? true : false;
                    rxData.dig[14] = ((data[3] & 0x40) > 0) ? true : false;
                    rxData.dig[15] = ((data[3] & 0x80) > 0) ? true : false;
                    rxData.dig[16] = ((data[4] & 0x1) > 0) ? true : false;
                    rxData.dig[17] = ((data[4] & 0x2) > 0) ? true : false;
                    rxData.dig[18] = ((data[4] & 0x4) > 0) ? true : false;
                    rxData.dig[19] = ((data[4] & 0x8) > 0) ? true : false;
                    rxData.hotOil = ((data[4] & 0x10) > 0) ? true : false;
                    rxData.coldOil = ((data[4] & 0x20) > 0) ? true : false;
                    rxData.lowOil = ((data[4] & 0x40) > 0) ? true : false;
                    rxData.lowLiquid = ((data[4] & 0x80) > 0) ? true : false;
                    rxData.dig[20] = ((data[5] & 0x1) > 0) ? true : false;
                    rxData.dig[21] = ((data[5] & 0x2) > 0) ? true : false;
                    rxData.dig[22] = ((data[5] & 0x4) > 0) ? true : false;
                    rxData.dig[23] = ((data[5] & 0x8) > 0) ? true : false;
                    rxData.dig[24] = ((data[5] & 0x10) > 0) ? true : false;
                    rxData.dig[25] = ((data[5] & 0x20) > 0) ? true : false;
                    rxData.dig[26] = ((data[5] & 0x40) > 0) ? true : false;
                    rxData.booms[0] = ((data[6] & 0x1) > 0) ? true : false;
                    rxData.booms[1] = ((data[6] & 0x2) > 0) ? true : false;
                    rxData.booms[2] = ((data[6] & 0x4) > 0) ? true : false;
                    rxData.booms[3] = ((data[6] & 0x8) > 0) ? true : false;
                    rxData.booms[4] = ((data[6] & 0x10) > 0) ? true : false;
                    rxData.booms[5] = ((data[6] & 0x20) > 0) ? true : false;
                    rxData.booms[6] = ((data[6] & 0x40) > 0) ? true : false;
                    rxData.booms[7] = ((data[6] & 0x80) > 0) ? true : false;
                    rxData.booms[8] = ((data[7] & 0x1) > 0) ? true : false;
                    rxData.booms[9] = ((data[7] & 0x2) > 0) ? true : false;
                    rxData.tankFlush = ((data[7] & 0x10) > 0) ? true : false;
                    break;
                case 0x5C:
                    rxData.packetCounter++;
                    buffer = Array.ConvertAll(data, item => (char)item);
                    rxData.liqName = new string(buffer, 0, 5);
                    break;
                case 0x5F:
                    rxData.packetCounter++;
                    rxData.ts = new DateTime(data[0] | (data[1] << 8), data[2], data[3], data[4], data[5], data[6], 0, System.DateTimeKind.Utc);
                    break;
                case 0x87:
                    if(data[1] > data[2])
                    {
                        rxData.senRoadTemp = pwm_obj_temp(data[1]);
                        rxData.senAirTemp = pwm_amb_temp(data[2]);
                    }
                    else
                    {
                        rxData.senRoadTemp = pwm_obj_temp(data[2]);
                        rxData.senAirTemp = pwm_amb_temp(data[1]);
                    }
                    rxData.senType = "MPP PWM";
                    break;
                case 0x100:
                    rxData.senSerial = String.Format("{0:X}:{1:X}:{2:X}:{3:X}:{4:X}:{5:X}", data[1], data[2], data[3], data[4], data[5], data[6]);
                    rxData.senVer = String.Format("{0}.{1}.{2}", data[5], data[4], data[3]);
                    rxData.senType = "MPP CAN";
                    break;
                case 0x101:
                    tempi = (int)data[0] | ((int)(data[1]) << 8);
                    rxData.senAirTemp = (tempi == 0xFFFF) ? 0xFFFF : Convert.ToDouble(tempi) * 0.1 - 60;
                    tempi = (int)data[2] | ((int)(data[3]) << 8);
                    rxData.senRoadTemp = (tempi == 0xFFFF) ? 0xFFFF : Convert.ToDouble(tempi) * 0.1 - 60;
                    tempi = (int)data[4] | ((int)(data[5]) << 8);
                    rxData.senPressure = (tempi == 0xFFFF) ? 0xFFFF : Convert.ToDouble(tempi) * 0.1;
                    tempi = (int)data[6] | ((int)(data[7]) << 8);
                    rxData.senHumidity = (tempi == 0xFFFF) ? 0xFFFF : Convert.ToDouble(tempi) * 0.1;
                    break;
                case 0x103:
                    rxData.senCfg = data[0] | (data[1] << 8);
                    break;
                default:
                    break;
            }
        }

        private void RBus_OnMessageRecieved(object sender, RS232_PACKET data)
        {
            if (!data.checksumValid)
            {
                return;
            }
            string[] parts = data.data.Split(',');
            int i = 0;
            rxData.packetCounter++;
            switch (data.id)
            {
                case "DAI":
                    if (parts.Length >= 9)
                    {
                        rxData.serial = parts[0];
                        rxData.ctrlVer = parts[3];
                        rxData.uiVer = parts[4];
                        rxData.lastCal = UnixToWindow(Convert.ToInt32(parts[5]));
                        rxData.truckId = parts[6];
                        rxData.systemRuntime = Convert.ToInt32(parts[7]);
                        rxData.protocolVersion = Convert.ToInt32(parts[8]);
                    }
                    break;
                case "DAR":
                    if (parts.Length >= 26)
                    {
                        i = 0;
                        if(rxData.protocolVersion != 0)
                        {
                            rxData.ts = new DateTime(Convert.ToInt32(parts[0]), Convert.ToInt32(parts[1]), Convert.ToInt32(parts[2]), Convert.ToInt32(parts[3]), Convert.ToInt32(parts[4]), 0, 0, System.DateTimeKind.Utc);
                            i = 5;
                        }

                        rxData.groundspeed = Convert.ToInt32(parts[i++]);
                        rxData.voltage = Convert.ToDouble(parts[i++]);
                        rxData.airTemp = Convert.ToDouble(parts[i++]);
                        rxData.roadTemp = Convert.ToDouble(parts[i++]);
                        rxData.pumpP = Convert.ToInt32(parts[i++]);
                        rxData.loadP = Convert.ToInt32(parts[i++]);
                        rxData.oilLevel = Convert.ToInt32(parts[i++]);
                        rxData.oilTemp = Convert.ToDouble(parts[i++]);
                        rxData.liqTankLevel = Convert.ToInt32(parts[i++]);
                        rxData.auto = parts[i++] == "1";
                        rxData.augerCl = parts[i++] == "1";
                        rxData.prewetCl = parts[i++] == "1";
                        rxData.pause = parts[i++] == "1";
                        rxData.blast = parts[i++] == "1";
                        rxData.spreaderMode = Convert.ToInt32(parts[i++]);
                        rxData.preWetMode = Convert.ToInt32(parts[i++]);
                        rxData.prodName = parts[i++];
                        if(rxData.protocolVersion != 0)
                        {
                            rxData.liqName = parts[i++];
                        }
                        rxData.route = parts[i++];
                        rxData.driver = parts[i++];
                        if(rxData.protocolVersion != 0)
                        {
                            rxData.totalDist = Convert.ToInt32(parts[i++]);
                            rxData.totalDistSpread = Convert.ToInt32(parts[i++]);
                            rxData.totalDistBlast = Convert.ToInt32(parts[i++]);
                        }
                        rxData.augerRate = Convert.ToInt32(parts[i++]);
                        rxData.auger2Rate = Convert.ToInt32(parts[i++]);
                        rxData.spinnerRate = Convert.ToInt32(parts[i++]);
                        rxData.preWetRate = Convert.ToInt32(parts[i++]);
                        rxData.antiIceRate = Convert.ToInt32(parts[i++]);
                        rxData.totalProd = Convert.ToInt32(parts[i++]);
                        rxData.totalLiq = Convert.ToInt32(parts[i++]);
                        if(rxData.protocolVersion != 0)
                        {
                            rxData.totalProdBlast = Convert.ToInt32(parts[i++]);
                        }
                    }
                    break;
                default:

                    break;
            }
        }

        private void JBus_OnMessageRecieved(object sender, J1708_PACKET data)
        {
            double temp;
            if (data.id == 200)
            {
                rxData.packetCounter++;
                rxData.senType = "Road Watch";
                if (data.data[0] == 171)
                {
                    temp = 0;
                    if (data.data[1] == 255)
                    {
                        temp = Convert.ToDouble(data.data[2] - 255) * .25;
                    }
                    else if (data.data[1] == 0)
                    {
                        temp = Convert.ToDouble(data.data[2]) * .25;
                    }
                    else if (data.data[1] == 1)
                    {
                        temp = Convert.ToDouble(data.data[2]) * .25 + 63;

                    }
                    temp -= 32;
                    temp *= (double)5 / 9;
                    rxData.senAirTemp = temp;
                }
                else if (data.data[0] == 255)
                {
                    temp = data.data[3];
                    temp -= 32;
                    temp *= (double)5 / 9;
                    rxData.senRoadTemp = temp;
                }
            }
        }

        private double pwm_obj_temp(int x)
        {
            return (((double)x - 143.44) * 1.1 - 20);
        }

        private double pwm_amb_temp(int x)
        {
            return (((double)x - 15.94) * 1.1 - 20);
        }

        private DateTime UnixToWindow(double ts)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(ts).ToLocalTime();
            return dtDateTime;
        }
    }

    class SysCanDataTx
    {
        public int[] ctrlRate = new int[3];


    }

    class SysBusDataRx
    {
        public string uiVer;
        public string ctrlVer;
        public string serial;
        public string truckId;
        public DateTime lastCal;
        public DateTime ts;
        public uint packetCounter;
        public int systemRuntime;
        public int protocolVersion;

        public int groundspeed;
        public double voltage;
        public double airTemp;
        public double roadTemp;
        public int pumpP;
        public int loadP;

        public bool auto;
        public bool augerCl;
        public bool prewetCl;
        public bool on;
        public bool pause;
        public bool blast;
        public bool laneMode;
        public int mode;
        public int spreaderMode;
        public int preWetMode;
        public int augerRate;
        public int auger2Rate;
        public int spinnerRate;
        public int[] spinnerDir = new int[2];
        public int preWetRate;
        public int antiIceRate;
        public int avgAuger;
        public int avgPreWet;
        public int avgAntiIce;

        public int totalDist;
        public int totalDistSpread;
        public int totalDistBlast;
        public int totalProdBlast;
        public int totalProd;
        public int totalLiq;
        public double P2PDist;
        public double P2PDistBlast;
        public double P2PProdOutput;
        public double P2PProdBlastOutput;
        public double P2PPreWetOutput;
        public double P2PAntiIceOutput;
        public int oilLevel;
        public double oilTemp;
        public string prodName;
        public string liqName;
        public string route;
        public string driver;

        public int liqTankLevel;
        public int bodyId;
        public bool[] dig = new bool[27];
        public bool[] booms = new bool[10];
        public bool tankFlush;
        public bool hotOil;
        public bool coldOil;
        public bool lowOil;
        public bool lowLiquid;

        public double senAirTemp;
        public double senRoadTemp;
        public double senHumidity;
        public double senPressure;
        public string senSerial;
        public string senVer;
        public string senType;
        public int senCfg;

        public SysBusDataRx()
        {
            uiVer = "";
            ctrlVer = "";
            serial = "";
            truckId = "";
            packetCounter = 0;
            systemRuntime = 0;
            protocolVersion = 0;
            groundspeed = 0;
            voltage = 0;
            airTemp = 0xFFF;
            roadTemp = 0xFFF;
            pumpP = 0;
            loadP = 0;
            auto = false;
            augerCl = false;
            prewetCl = false;
            on = false;
            pause = false;
            blast = false;
            laneMode = false;
            mode = 0;
            spreaderMode = 0;
            preWetMode = 0;
            augerRate = 0;
            auger2Rate = 0;
            spinnerRate = 0;
            spinnerDir[0] = 0;
            spinnerDir[1] = 0;
            preWetRate = 0;
            antiIceRate = 0;
            avgAuger = 0;
            avgPreWet = 0;
            avgAntiIce = 0;
            totalDist = 0;
            totalDistSpread = 0;
            totalDistBlast = 0;
            totalProd = 0;
            totalProdBlast = 0;
            totalLiq = 0;
            P2PDist = 0;
            P2PDistBlast = 0;
            P2PProdOutput = 0;
            P2PProdBlastOutput = 0;
            P2PPreWetOutput = 0;
            P2PAntiIceOutput = 0;
            oilLevel = 0xFF;
            oilTemp = 0xFFF;
            prodName = "";
            liqName = "";
            driver = "";
            route = "";
            liqTankLevel = 0xFF;
            bodyId = 0;
            tankFlush = false;
            hotOil = false;
            coldOil = false;
            lowOil = false;
            lowLiquid = false;
            senAirTemp = 0xFFFF;
            senRoadTemp = 0xFFFF;
            senHumidity = 0xFFFF;
            senPressure = 0xFFFF;
            senSerial = "";
            senVer = "";
            senCfg = 0;
        }
    }
}
