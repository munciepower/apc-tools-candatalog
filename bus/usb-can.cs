﻿/*!
 * @file usb-can.cs
 * @project Adv+ AVL Test Application
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @note Compiled with Visual Studio 2015
 * @copyright 2019 Muncie Power Products
 * @section DESCRIPTION
 * This file contains a can-bus interface class designed to work with SysTec's
 * USB-CANmodul
 * 
 * @section LICENSE
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * 1) Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 *   
 * 2) Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.
 *   
 * 3) Neither the name of the copyright holder nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.Text;
using UcanDotNET;

namespace ADV_CAN_Datalogging
{
    class usb_can
    {

        private USBcanServer server = new USBcanServer();
        public event EventHandler OnMessageRecieved;
        private bool running;

        public usb_can()
        {
            server.CanMsgReceivedEvent += OnRxMessage;
        }

        public bool Start()
        {
            byte ret = server.InitHardware(UcanDotNET.USBcanServer.USBCAN_ANY_MODULE);
            if (ret != (byte)USBcanServer.eUcanReturn.USBCAN_SUCCESSFUL)
            {
                return false;
            }
            ret = server.InitCan((byte)USBcanServer.eUcanChannel.USBCAN_CHANNEL_CH0,
            (short)USBcanServer.eUcanBaudrate.USBCAN_BAUD_250kBit,
            (int)USBcanServer.eUcanBaudrateEx.USBCAN_BAUDEX_250kBit);

            if (ret == (byte)USBcanServer.eUcanReturn.USBCAN_SUCCESSFUL)
            {
                running = true;
                return true;
            }
            return false;
        }

        public bool Stop()
        {
            byte ret = server.Shutdown(0, true);
            if (ret == (byte)USBcanServer.eUcanReturn.USBCAN_SUCCESSFUL)
            {
                running = false;
                return true;
            }
            return false;
        }

        public bool IsRunning()
        {
            return running;
        }

        public void sendTx(CAN_PACKET p)
        {
            USBcanServer.tCanMsgStruct[] msg = new USBcanServer.tCanMsgStruct[1];
            msg[0].m_bData = new byte[8];

            msg[0].m_dwID = p.id;
            msg[0].m_bDLC = p.len;
            for(int i = 0; i < p.len; i++)
            {
                msg[0].m_bData[i] = Convert.ToByte(p.data[i]);
            }
            int count = 1;
            server.WriteCanMsg(0, ref msg, ref count);
        }

        private void OnRxMessage(byte bDeviceNr_p, byte bChannel_p)
        {
            USBcanServer.tCanMsgStruct[] msgs = new USBcanServer.tCanMsgStruct[20];
            byte channel = 0;
            int count = 0;

            server.ReadCanMsg(ref channel, ref msgs, ref count);

            for (int i = 0; i < count; i++)
            {
                PacketEventArgs args = new PacketEventArgs();
                args.packet = new ADV_CAN_Datalogging.CAN_PACKET();
                USBcanServer.tCanMsgStruct msg = msgs[i];
                args.packet.id = msg.m_dwID;
                args.packet.len = msg.m_bDLC;
                for(int j = 0; j < args.packet.len; j++)
                {
                    args.packet.data[j] = Convert.ToInt32(msg.m_bData[j]);
                }
                OnMessageRecieved?.Invoke(this, args);
            }  
        }
    }

    public class CAN_PACKET
    {
        public int id;
        public byte len;
        public int[] data = new int[8];
    }

    /*!< Action Event Args Class */
    public class PacketEventArgs : EventArgs
    {
        public CAN_PACKET packet { get; set; }
    }
}
