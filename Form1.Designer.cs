﻿/*!
 * @file Form1.Designer.cs
 * @project Adv+ AVL Test Application
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @note Compiled with Visual Studio 2015
 * @copyright 2019 Muncie Power Products
 * @section DESCRIPTION
 * This file contains the UI definition for Form1
 * 
 * @section LICENSE
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * 1) Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 *   
 * 2) Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.
 *   
 * 3) Neither the name of the copyright holder nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE.
 */
namespace ADV_CAN_Datalogging
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPanel = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dy_ts = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.packets_rx = new System.Windows.Forms.Label();
            this.label86 = new System.Windows.Forms.Label();
            this.rs232_status = new System.Windows.Forms.Label();
            this.label82 = new System.Windows.Forms.Label();
            this.dy_protoVer = new System.Windows.Forms.Label();
            this.label81 = new System.Windows.Forms.Label();
            this.j1708_status = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.can_status = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dy_ctrlVer = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dy_serial = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.dy_uiVer = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dy_truckId = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dy_lastCal = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.dy_voltage = new System.Windows.Forms.Label();
            this.dy_gspeed = new System.Windows.Forms.Label();
            this.dy_airTemp = new System.Windows.Forms.Label();
            this.dy_roadTemp = new System.Windows.Forms.Label();
            this.dy_pumpPress = new System.Windows.Forms.Label();
            this.dy_loadPress = new System.Windows.Forms.Label();
            this.dy_oillvl = new System.Windows.Forms.Label();
            this.dy_oilT = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.dy_runtime = new System.Windows.Forms.Label();
            this.dy_route = new System.Windows.Forms.Label();
            this.dy_driver = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.dy_coldOil = new System.Windows.Forms.Label();
            this.dy_hotOil = new System.Windows.Forms.Label();
            this.dy_lowOil = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label60 = new System.Windows.Forms.Label();
            this.label83 = new System.Windows.Forms.Label();
            this.label84 = new System.Windows.Forms.Label();
            this.label85 = new System.Windows.Forms.Label();
            this.dy_spreaderM2 = new System.Windows.Forms.Label();
            this.rmt_rate1 = new System.Windows.Forms.NumericUpDown();
            this.rmt_rate3_on = new System.Windows.Forms.CheckBox();
            this.rmt_rate1_on = new System.Windows.Forms.CheckBox();
            this.rmt_rate2_on = new System.Windows.Forms.CheckBox();
            this.rmt_rate2 = new System.Windows.Forms.NumericUpDown();
            this.rmt_rate3 = new System.Windows.Forms.NumericUpDown();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.dy_dist_sprd = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.dy_auger2R = new System.Windows.Forms.Label();
            this.dy_augerR = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.dy_spreaderM = new System.Windows.Forms.Label();
            this.dy_spinR = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.dy_augAvg = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.dy_augerCl = new System.Windows.Forms.Label();
            this.dy_miles = new System.Windows.Forms.Label();
            this.dy_milesB = new System.Windows.Forms.Label();
            this.dy_prodN = new System.Windows.Forms.Label();
            this.dy_blast = new System.Windows.Forms.Label();
            this.dy_pause = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.dy_blastO = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.dy_autoMode = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.dy_prodO = new System.Windows.Forms.Label();
            this.dy_prodT = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.dy_bodyID = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.dy_laneMode = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.dy_liqName = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.dy_liqT = new System.Windows.Forms.Label();
            this.dy_aiR = new System.Windows.Forms.Label();
            this.dy_pwR = new System.Windows.Forms.Label();
            this.dy_pwAvg = new System.Windows.Forms.Label();
            this.dy_aiO = new System.Windows.Forms.Label();
            this.dy_pwO = new System.Windows.Forms.Label();
            this.dy_aiAvg = new System.Windows.Forms.Label();
            this.dy_pwM = new System.Windows.Forms.Label();
            this.dy_pwCl = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.dy_liquidTankLvl = new System.Windows.Forms.Label();
            this.dy_lowLiq = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label91 = new System.Windows.Forms.Label();
            this.label92 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.dy_boom1 = new System.Windows.Forms.Label();
            this.dy_boom2 = new System.Windows.Forms.Label();
            this.dy_boom3 = new System.Windows.Forms.Label();
            this.dy_boom4 = new System.Windows.Forms.Label();
            this.dy_boom5 = new System.Windows.Forms.Label();
            this.dy_boom6 = new System.Windows.Forms.Label();
            this.dy_boom7 = new System.Windows.Forms.Label();
            this.dy_boom8 = new System.Windows.Forms.Label();
            this.dy_boom9 = new System.Windows.Forms.Label();
            this.dy_boom10 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label48 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label56 = new System.Windows.Forms.Label();
            this.dy_dig3 = new System.Windows.Forms.Label();
            this.dy_dig2 = new System.Windows.Forms.Label();
            this.dy_dig4 = new System.Windows.Forms.Label();
            this.dy_dig6 = new System.Windows.Forms.Label();
            this.dy_dig7 = new System.Windows.Forms.Label();
            this.dy_dig5 = new System.Windows.Forms.Label();
            this.dy_dig1 = new System.Windows.Forms.Label();
            this.dy_dig13 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.dy_dig14 = new System.Windows.Forms.Label();
            this.dy_dig15 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.label71 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label76 = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.label75 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.dy_dig8 = new System.Windows.Forms.Label();
            this.dy_dig9 = new System.Windows.Forms.Label();
            this.dy_dig10 = new System.Windows.Forms.Label();
            this.dy_dig11 = new System.Windows.Forms.Label();
            this.dy_dig12 = new System.Windows.Forms.Label();
            this.dy_dig16 = new System.Windows.Forms.Label();
            this.dy_dig17 = new System.Windows.Forms.Label();
            this.dy_dig18 = new System.Windows.Forms.Label();
            this.dy_dig19 = new System.Windows.Forms.Label();
            this.dy_dig20 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label80 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.dy_dig21 = new System.Windows.Forms.Label();
            this.dy_dig22 = new System.Windows.Forms.Label();
            this.dy_dig23 = new System.Windows.Forms.Label();
            this.dy_dig24 = new System.Windows.Forms.Label();
            this.dy_dig25 = new System.Windows.Forms.Label();
            this.dy_dig26 = new System.Windows.Forms.Label();
            this.dy_dig27 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.dy_arts_sen = new System.Windows.Forms.Label();
            this.label89 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.dy_arts_ver = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.dy_arts_serial = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.dy_arts_obj = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.dy_arts_air = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.dy_arts_humidity = new System.Windows.Forms.Label();
            this.dy_arts_pressure = new System.Windows.Forms.Label();
            this.dy_arts_cfg = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startCANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopCANToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startRS232 = new System.Windows.Forms.ToolStripMenuItem();
            this.stopRS232 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.startRoadWatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopRoadwatchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label105 = new System.Windows.Forms.Label();
            this.dy_tankFlush = new System.Windows.Forms.Label();
            this.tabPanel.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rmt_rate1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rmt_rate2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rmt_rate3)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPanel
            // 
            this.tabPanel.Controls.Add(this.tabPage1);
            this.tabPanel.Controls.Add(this.tabPage6);
            this.tabPanel.Controls.Add(this.tabPage2);
            this.tabPanel.Controls.Add(this.tabPage3);
            this.tabPanel.Controls.Add(this.tabPage4);
            this.tabPanel.Controls.Add(this.tabPage5);
            this.tabPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPanel.Location = new System.Drawing.Point(0, 24);
            this.tabPanel.Name = "tabPanel";
            this.tabPanel.SelectedIndex = 0;
            this.tabPanel.Size = new System.Drawing.Size(920, 552);
            this.tabPanel.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(912, 526);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "General";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.dy_ts, 3, 13);
            this.tableLayoutPanel1.Controls.Add(this.label104, 2, 13);
            this.tableLayoutPanel1.Controls.Add(this.packets_rx, 3, 12);
            this.tableLayoutPanel1.Controls.Add(this.label86, 2, 12);
            this.tableLayoutPanel1.Controls.Add(this.rs232_status, 1, 13);
            this.tableLayoutPanel1.Controls.Add(this.label82, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.dy_protoVer, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label81, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.j1708_status, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.label59, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.can_status, 1, 11);
            this.tableLayoutPanel1.Controls.Add(this.label42, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.dy_ctrlVer, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.dy_serial, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.dy_uiVer, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.dy_truckId, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.dy_lastCal, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label10, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label11, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label12, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.label34, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.label37, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.dy_voltage, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.dy_gspeed, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.dy_airTemp, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.dy_roadTemp, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.dy_pumpPress, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.dy_loadPress, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.dy_oillvl, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.dy_oilT, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.label20, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label39, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label40, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.dy_runtime, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.dy_route, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.dy_driver, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.label44, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.label45, 2, 9);
            this.tableLayoutPanel1.Controls.Add(this.label46, 2, 10);
            this.tableLayoutPanel1.Controls.Add(this.dy_coldOil, 3, 8);
            this.tableLayoutPanel1.Controls.Add(this.dy_hotOil, 3, 9);
            this.tableLayoutPanel1.Controls.Add(this.dy_lowOil, 3, 10);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 15;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1224, 465);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // dy_ts
            // 
            this.dy_ts.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.dy_ts, 2);
            this.dy_ts.Cursor = System.Windows.Forms.Cursors.No;
            this.dy_ts.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_ts.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_ts.Location = new System.Drawing.Point(562, 338);
            this.dy_ts.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_ts.Name = "dy_ts";
            this.dy_ts.Size = new System.Drawing.Size(329, 26);
            this.dy_ts.TabIndex = 114;
            this.dy_ts.Text = "{val}";
            this.dy_ts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(362, 338);
            this.label104.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(196, 26);
            this.label104.TabIndex = 113;
            this.label104.Text = "Last Packet";
            this.label104.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // packets_rx
            // 
            this.packets_rx.AutoSize = true;
            this.packets_rx.Dock = System.Windows.Forms.DockStyle.Fill;
            this.packets_rx.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.packets_rx.Location = new System.Drawing.Point(562, 312);
            this.packets_rx.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.packets_rx.Name = "packets_rx";
            this.packets_rx.Size = new System.Drawing.Size(129, 26);
            this.packets_rx.TabIndex = 112;
            this.packets_rx.Text = "0";
            this.packets_rx.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.packets_rx.UseCompatibleTextRendering = true;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(362, 312);
            this.label86.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(196, 26);
            this.label86.TabIndex = 111;
            this.label86.Text = "Packets RXd";
            this.label86.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // rs232_status
            // 
            this.rs232_status.AutoSize = true;
            this.rs232_status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rs232_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rs232_status.Location = new System.Drawing.Point(202, 338);
            this.rs232_status.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.rs232_status.Name = "rs232_status";
            this.rs232_status.Size = new System.Drawing.Size(156, 26);
            this.rs232_status.TabIndex = 110;
            this.rs232_status.Text = "Off";
            this.rs232_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(2, 338);
            this.label82.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(196, 26);
            this.label82.TabIndex = 109;
            this.label82.Text = "RS232 Status";
            this.label82.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_protoVer
            // 
            this.dy_protoVer.AutoSize = true;
            this.dy_protoVer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_protoVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_protoVer.Location = new System.Drawing.Point(202, 208);
            this.dy_protoVer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_protoVer.Name = "dy_protoVer";
            this.dy_protoVer.Size = new System.Drawing.Size(156, 26);
            this.dy_protoVer.TabIndex = 108;
            this.dy_protoVer.Text = "{val}";
            this.dy_protoVer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(2, 208);
            this.label81.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(196, 26);
            this.label81.TabIndex = 107;
            this.label81.Text = "Protocol Ver";
            this.label81.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // j1708_status
            // 
            this.j1708_status.AutoSize = true;
            this.j1708_status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.j1708_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.j1708_status.Location = new System.Drawing.Point(202, 312);
            this.j1708_status.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.j1708_status.Name = "j1708_status";
            this.j1708_status.Size = new System.Drawing.Size(156, 26);
            this.j1708_status.TabIndex = 106;
            this.j1708_status.Text = "Off";
            this.j1708_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(2, 312);
            this.label59.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(196, 26);
            this.label59.TabIndex = 105;
            this.label59.Text = "J1708 Status";
            this.label59.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // can_status
            // 
            this.can_status.AutoSize = true;
            this.can_status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.can_status.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.can_status.Location = new System.Drawing.Point(202, 286);
            this.can_status.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.can_status.Name = "can_status";
            this.can_status.Size = new System.Drawing.Size(156, 26);
            this.can_status.TabIndex = 98;
            this.can_status.Text = "Off";
            this.can_status.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(2, 286);
            this.label42.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(196, 26);
            this.label42.TabIndex = 97;
            this.label42.Text = "CAN Status";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 0);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(196, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "CTRL Version";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_ctrlVer
            // 
            this.dy_ctrlVer.AutoSize = true;
            this.dy_ctrlVer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_ctrlVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_ctrlVer.Location = new System.Drawing.Point(202, 0);
            this.dy_ctrlVer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_ctrlVer.Name = "dy_ctrlVer";
            this.dy_ctrlVer.Size = new System.Drawing.Size(156, 26);
            this.dy_ctrlVer.TabIndex = 40;
            this.dy_ctrlVer.Text = "{val}";
            this.dy_ctrlVer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(2, 52);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(196, 26);
            this.label6.TabIndex = 5;
            this.label6.Text = "Serial";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_serial
            // 
            this.dy_serial.AutoSize = true;
            this.dy_serial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_serial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_serial.Location = new System.Drawing.Point(202, 52);
            this.dy_serial.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_serial.Name = "dy_serial";
            this.dy_serial.Size = new System.Drawing.Size(156, 26);
            this.dy_serial.TabIndex = 44;
            this.dy_serial.Text = "{val}";
            this.dy_serial.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 26);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(196, 26);
            this.label5.TabIndex = 4;
            this.label5.Text = "UI Version";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_uiVer
            // 
            this.dy_uiVer.AutoSize = true;
            this.dy_uiVer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_uiVer.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_uiVer.Location = new System.Drawing.Point(202, 26);
            this.dy_uiVer.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_uiVer.Name = "dy_uiVer";
            this.dy_uiVer.Size = new System.Drawing.Size(156, 26);
            this.dy_uiVer.TabIndex = 41;
            this.dy_uiVer.Text = "{val}";
            this.dy_uiVer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 78);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(196, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Truck ID";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_truckId
            // 
            this.dy_truckId.AutoSize = true;
            this.dy_truckId.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_truckId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_truckId.Location = new System.Drawing.Point(202, 78);
            this.dy_truckId.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_truckId.Name = "dy_truckId";
            this.dy_truckId.Size = new System.Drawing.Size(156, 26);
            this.dy_truckId.TabIndex = 45;
            this.dy_truckId.Text = "{val}";
            this.dy_truckId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 104);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(196, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Last Cal";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_lastCal
            // 
            this.dy_lastCal.AutoSize = true;
            this.dy_lastCal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_lastCal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_lastCal.Location = new System.Drawing.Point(202, 104);
            this.dy_lastCal.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_lastCal.Name = "dy_lastCal";
            this.dy_lastCal.Size = new System.Drawing.Size(156, 26);
            this.dy_lastCal.TabIndex = 42;
            this.dy_lastCal.Text = "{val}";
            this.dy_lastCal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(362, 0);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(196, 26);
            this.label8.TabIndex = 7;
            this.label8.Text = "Voltage";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(362, 26);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(196, 26);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ground Speed";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(362, 52);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(196, 26);
            this.label9.TabIndex = 8;
            this.label9.Text = "Air Temp";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(362, 78);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(196, 26);
            this.label10.TabIndex = 9;
            this.label10.Text = "Road Temp";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(362, 104);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(196, 26);
            this.label11.TabIndex = 10;
            this.label11.Text = "Pump Press";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(362, 130);
            this.label12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(196, 26);
            this.label12.TabIndex = 11;
            this.label12.Text = "Load Press";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(362, 156);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(196, 26);
            this.label34.TabIndex = 34;
            this.label34.Text = "Tank Oil Lvl";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(362, 182);
            this.label37.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(196, 26);
            this.label37.TabIndex = 37;
            this.label37.Text = "Tank Oil Temp";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_voltage
            // 
            this.dy_voltage.AutoSize = true;
            this.dy_voltage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_voltage.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_voltage.Location = new System.Drawing.Point(562, 0);
            this.dy_voltage.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_voltage.Name = "dy_voltage";
            this.dy_voltage.Size = new System.Drawing.Size(129, 26);
            this.dy_voltage.TabIndex = 48;
            this.dy_voltage.Text = "{val}";
            this.dy_voltage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_gspeed
            // 
            this.dy_gspeed.AutoSize = true;
            this.dy_gspeed.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_gspeed.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_gspeed.Location = new System.Drawing.Point(562, 26);
            this.dy_gspeed.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_gspeed.Name = "dy_gspeed";
            this.dy_gspeed.Size = new System.Drawing.Size(129, 26);
            this.dy_gspeed.TabIndex = 46;
            this.dy_gspeed.Text = "{val}";
            this.dy_gspeed.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_airTemp
            // 
            this.dy_airTemp.AutoSize = true;
            this.dy_airTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_airTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_airTemp.Location = new System.Drawing.Point(562, 52);
            this.dy_airTemp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_airTemp.Name = "dy_airTemp";
            this.dy_airTemp.Size = new System.Drawing.Size(129, 26);
            this.dy_airTemp.TabIndex = 50;
            this.dy_airTemp.Text = "{val}";
            this.dy_airTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_roadTemp
            // 
            this.dy_roadTemp.AutoSize = true;
            this.dy_roadTemp.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_roadTemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_roadTemp.Location = new System.Drawing.Point(562, 78);
            this.dy_roadTemp.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_roadTemp.Name = "dy_roadTemp";
            this.dy_roadTemp.Size = new System.Drawing.Size(129, 26);
            this.dy_roadTemp.TabIndex = 49;
            this.dy_roadTemp.Text = "{val}";
            this.dy_roadTemp.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_pumpPress
            // 
            this.dy_pumpPress.AutoSize = true;
            this.dy_pumpPress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_pumpPress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_pumpPress.Location = new System.Drawing.Point(562, 104);
            this.dy_pumpPress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_pumpPress.Name = "dy_pumpPress";
            this.dy_pumpPress.Size = new System.Drawing.Size(129, 26);
            this.dy_pumpPress.TabIndex = 52;
            this.dy_pumpPress.Text = "{val}";
            this.dy_pumpPress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_loadPress
            // 
            this.dy_loadPress.AutoSize = true;
            this.dy_loadPress.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_loadPress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_loadPress.Location = new System.Drawing.Point(562, 130);
            this.dy_loadPress.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_loadPress.Name = "dy_loadPress";
            this.dy_loadPress.Size = new System.Drawing.Size(129, 26);
            this.dy_loadPress.TabIndex = 53;
            this.dy_loadPress.Text = "{val}";
            this.dy_loadPress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_oillvl
            // 
            this.dy_oillvl.AutoSize = true;
            this.dy_oillvl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_oillvl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_oillvl.Location = new System.Drawing.Point(562, 156);
            this.dy_oillvl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_oillvl.Name = "dy_oillvl";
            this.dy_oillvl.Size = new System.Drawing.Size(129, 26);
            this.dy_oillvl.TabIndex = 84;
            this.dy_oillvl.Text = "{val}";
            this.dy_oillvl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_oilT
            // 
            this.dy_oilT.AutoSize = true;
            this.dy_oilT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_oilT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_oilT.Location = new System.Drawing.Point(562, 182);
            this.dy_oilT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_oilT.Name = "dy_oilT";
            this.dy_oilT.Size = new System.Drawing.Size(129, 26);
            this.dy_oilT.TabIndex = 90;
            this.dy_oilT.Text = "{val}";
            this.dy_oilT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(2, 130);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(196, 26);
            this.label20.TabIndex = 91;
            this.label20.Text = "System Runtime";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(2, 156);
            this.label39.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(196, 26);
            this.label39.TabIndex = 92;
            this.label39.Text = "Route ID";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(2, 182);
            this.label40.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(196, 26);
            this.label40.TabIndex = 93;
            this.label40.Text = "Driver ID";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_runtime
            // 
            this.dy_runtime.AutoSize = true;
            this.dy_runtime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_runtime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_runtime.Location = new System.Drawing.Point(202, 130);
            this.dy_runtime.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_runtime.Name = "dy_runtime";
            this.dy_runtime.Size = new System.Drawing.Size(156, 26);
            this.dy_runtime.TabIndex = 94;
            this.dy_runtime.Text = "{val}";
            this.dy_runtime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_route
            // 
            this.dy_route.AutoSize = true;
            this.dy_route.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_route.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_route.Location = new System.Drawing.Point(202, 156);
            this.dy_route.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_route.Name = "dy_route";
            this.dy_route.Size = new System.Drawing.Size(156, 26);
            this.dy_route.TabIndex = 95;
            this.dy_route.Text = "{val}";
            this.dy_route.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_driver
            // 
            this.dy_driver.AutoSize = true;
            this.dy_driver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_driver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_driver.Location = new System.Drawing.Point(202, 182);
            this.dy_driver.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_driver.Name = "dy_driver";
            this.dy_driver.Size = new System.Drawing.Size(156, 26);
            this.dy_driver.TabIndex = 96;
            this.dy_driver.Text = "{val}";
            this.dy_driver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(362, 208);
            this.label44.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(196, 26);
            this.label44.TabIndex = 99;
            this.label44.Text = "Cold Oil Flag";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(362, 234);
            this.label45.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(196, 26);
            this.label45.TabIndex = 100;
            this.label45.Text = "Hot Oil Flag";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(362, 260);
            this.label46.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(196, 26);
            this.label46.TabIndex = 101;
            this.label46.Text = "Low Oil Flag";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_coldOil
            // 
            this.dy_coldOil.AutoSize = true;
            this.dy_coldOil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_coldOil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_coldOil.Location = new System.Drawing.Point(562, 208);
            this.dy_coldOil.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_coldOil.Name = "dy_coldOil";
            this.dy_coldOil.Size = new System.Drawing.Size(129, 26);
            this.dy_coldOil.TabIndex = 102;
            this.dy_coldOil.Text = "{val}";
            this.dy_coldOil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_hotOil
            // 
            this.dy_hotOil.AutoSize = true;
            this.dy_hotOil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_hotOil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_hotOil.Location = new System.Drawing.Point(562, 234);
            this.dy_hotOil.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_hotOil.Name = "dy_hotOil";
            this.dy_hotOil.Size = new System.Drawing.Size(129, 26);
            this.dy_hotOil.TabIndex = 103;
            this.dy_hotOil.Text = "{val}";
            this.dy_hotOil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_lowOil
            // 
            this.dy_lowOil.AutoSize = true;
            this.dy_lowOil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_lowOil.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_lowOil.Location = new System.Drawing.Point(562, 260);
            this.dy_lowOil.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_lowOil.Name = "dy_lowOil";
            this.dy_lowOil.Size = new System.Drawing.Size(129, 26);
            this.dy_lowOil.TabIndex = 104;
            this.dy_lowOil.Text = "{val}";
            this.dy_lowOil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.tableLayoutPanel6);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(912, 526);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Remote Control";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 5;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.label60, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.label83, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label84, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label85, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.dy_spreaderM2, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.rmt_rate1, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.rmt_rate3_on, 2, 3);
            this.tableLayoutPanel6.Controls.Add(this.rmt_rate1_on, 2, 1);
            this.tableLayoutPanel6.Controls.Add(this.rmt_rate2_on, 2, 2);
            this.tableLayoutPanel6.Controls.Add(this.rmt_rate2, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.rmt_rate3, 1, 3);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 16;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(906, 520);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(2, 90);
            this.label60.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(196, 30);
            this.label60.TabIndex = 69;
            this.label60.Text = "Control #3 Rate";
            this.label60.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(2, 60);
            this.label83.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(196, 30);
            this.label83.TabIndex = 22;
            this.label83.Text = "Control #2 Rate";
            this.label83.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(2, 30);
            this.label84.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(196, 30);
            this.label84.TabIndex = 21;
            this.label84.Text = "Control #1 Rate";
            this.label84.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(2, 0);
            this.label85.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(196, 30);
            this.label85.TabIndex = 19;
            this.label85.Text = "Spreader Mode";
            this.label85.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_spreaderM2
            // 
            this.dy_spreaderM2.AutoSize = true;
            this.dy_spreaderM2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_spreaderM2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_spreaderM2.Location = new System.Drawing.Point(202, 0);
            this.dy_spreaderM2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_spreaderM2.Name = "dy_spreaderM2";
            this.dy_spreaderM2.Size = new System.Drawing.Size(146, 30);
            this.dy_spreaderM2.TabIndex = 67;
            this.dy_spreaderM2.Text = "{val}";
            this.dy_spreaderM2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // rmt_rate1
            // 
            this.rmt_rate1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rmt_rate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmt_rate1.Location = new System.Drawing.Point(203, 33);
            this.rmt_rate1.Name = "rmt_rate1";
            this.rmt_rate1.Size = new System.Drawing.Size(144, 24);
            this.rmt_rate1.TabIndex = 72;
            // 
            // rmt_rate3_on
            // 
            this.rmt_rate3_on.Appearance = System.Windows.Forms.Appearance.Button;
            this.rmt_rate3_on.AutoSize = true;
            this.rmt_rate3_on.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rmt_rate3_on.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmt_rate3_on.Location = new System.Drawing.Point(353, 93);
            this.rmt_rate3_on.Name = "rmt_rate3_on";
            this.rmt_rate3_on.Size = new System.Drawing.Size(94, 24);
            this.rmt_rate3_on.TabIndex = 71;
            this.rmt_rate3_on.Text = "Off";
            this.rmt_rate3_on.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rmt_rate3_on.UseVisualStyleBackColor = true;
            this.rmt_rate3_on.CheckedChanged += new System.EventHandler(this.rmt_rate_CheckedChanged);
            // 
            // rmt_rate1_on
            // 
            this.rmt_rate1_on.Appearance = System.Windows.Forms.Appearance.Button;
            this.rmt_rate1_on.AutoSize = true;
            this.rmt_rate1_on.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rmt_rate1_on.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmt_rate1_on.Location = new System.Drawing.Point(353, 33);
            this.rmt_rate1_on.Name = "rmt_rate1_on";
            this.rmt_rate1_on.Size = new System.Drawing.Size(94, 24);
            this.rmt_rate1_on.TabIndex = 73;
            this.rmt_rate1_on.Text = "Off";
            this.rmt_rate1_on.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rmt_rate1_on.UseVisualStyleBackColor = true;
            this.rmt_rate1_on.CheckedChanged += new System.EventHandler(this.rmt_rate_CheckedChanged);
            // 
            // rmt_rate2_on
            // 
            this.rmt_rate2_on.Appearance = System.Windows.Forms.Appearance.Button;
            this.rmt_rate2_on.AutoSize = true;
            this.rmt_rate2_on.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rmt_rate2_on.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmt_rate2_on.Location = new System.Drawing.Point(353, 63);
            this.rmt_rate2_on.Name = "rmt_rate2_on";
            this.rmt_rate2_on.Size = new System.Drawing.Size(94, 24);
            this.rmt_rate2_on.TabIndex = 74;
            this.rmt_rate2_on.Text = "Off";
            this.rmt_rate2_on.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rmt_rate2_on.UseVisualStyleBackColor = true;
            this.rmt_rate2_on.CheckedChanged += new System.EventHandler(this.rmt_rate_CheckedChanged);
            // 
            // rmt_rate2
            // 
            this.rmt_rate2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rmt_rate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmt_rate2.Location = new System.Drawing.Point(203, 63);
            this.rmt_rate2.Name = "rmt_rate2";
            this.rmt_rate2.Size = new System.Drawing.Size(144, 24);
            this.rmt_rate2.TabIndex = 75;
            // 
            // rmt_rate3
            // 
            this.rmt_rate3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rmt_rate3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rmt_rate3.Location = new System.Drawing.Point(203, 93);
            this.rmt_rate3.Name = "rmt_rate3";
            this.rmt_rate3.Size = new System.Drawing.Size(144, 24);
            this.rmt_rate3.TabIndex = 76;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(912, 526);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Spreader";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.dy_dist_sprd, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.label87, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label22, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.dy_auger2R, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.dy_augerR, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label21, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.dy_spreaderM, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.dy_spinR, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label25, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.dy_augAvg, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.label14, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label28, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label29, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.dy_augerCl, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.dy_miles, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.dy_milesB, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.dy_prodN, 3, 6);
            this.tableLayoutPanel2.Controls.Add(this.dy_blast, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.dy_pause, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.label31, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.dy_blastO, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.label13, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.dy_autoMode, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.label17, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.label16, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.label38, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.label30, 0, 9);
            this.tableLayoutPanel2.Controls.Add(this.label35, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.dy_prodO, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.dy_prodT, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.label43, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.dy_bodyID, 3, 8);
            this.tableLayoutPanel2.Controls.Add(this.label47, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.dy_laneMode, 3, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 16;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(906, 520);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // dy_dist_sprd
            // 
            this.dy_dist_sprd.AutoSize = true;
            this.dy_dist_sprd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dist_sprd.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dist_sprd.Location = new System.Drawing.Point(202, 180);
            this.dy_dist_sprd.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dist_sprd.Name = "dy_dist_sprd";
            this.dy_dist_sprd.Size = new System.Drawing.Size(156, 30);
            this.dy_dist_sprd.TabIndex = 113;
            this.dy_dist_sprd.Text = "{val}";
            this.dy_dist_sprd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(2, 180);
            this.label87.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(196, 30);
            this.label87.TabIndex = 112;
            this.label87.Text = "Distance Spread";
            this.label87.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(2, 90);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(196, 30);
            this.label22.TabIndex = 69;
            this.label22.Text = "Spinner Rate";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_auger2R
            // 
            this.dy_auger2R.AutoSize = true;
            this.dy_auger2R.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_auger2R.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_auger2R.Location = new System.Drawing.Point(202, 60);
            this.dy_auger2R.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_auger2R.Name = "dy_auger2R";
            this.dy_auger2R.Size = new System.Drawing.Size(156, 30);
            this.dy_auger2R.TabIndex = 68;
            this.dy_auger2R.Text = "{val}";
            this.dy_auger2R.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_augerR
            // 
            this.dy_augerR.AutoSize = true;
            this.dy_augerR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_augerR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_augerR.Location = new System.Drawing.Point(202, 30);
            this.dy_augerR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_augerR.Name = "dy_augerR";
            this.dy_augerR.Size = new System.Drawing.Size(156, 30);
            this.dy_augerR.TabIndex = 66;
            this.dy_augerR.Text = "{val}";
            this.dy_augerR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(2, 60);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(196, 30);
            this.label21.TabIndex = 22;
            this.label21.Text = "Auger 2 Rate";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(2, 30);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(196, 30);
            this.label7.TabIndex = 21;
            this.label7.Text = "Auger Rate";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(2, 0);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(196, 30);
            this.label18.TabIndex = 19;
            this.label18.Text = "Spreader Mode";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_spreaderM
            // 
            this.dy_spreaderM.AutoSize = true;
            this.dy_spreaderM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_spreaderM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_spreaderM.Location = new System.Drawing.Point(202, 0);
            this.dy_spreaderM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_spreaderM.Name = "dy_spreaderM";
            this.dy_spreaderM.Size = new System.Drawing.Size(156, 30);
            this.dy_spreaderM.TabIndex = 67;
            this.dy_spreaderM.Text = "{val}";
            this.dy_spreaderM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_spinR
            // 
            this.dy_spinR.AutoSize = true;
            this.dy_spinR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_spinR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_spinR.Location = new System.Drawing.Point(202, 90);
            this.dy_spinR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_spinR.Name = "dy_spinR";
            this.dy_spinR.Size = new System.Drawing.Size(156, 30);
            this.dy_spinR.TabIndex = 70;
            this.dy_spinR.Text = "{val}";
            this.dy_spinR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(2, 120);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(196, 30);
            this.label25.TabIndex = 73;
            this.label25.Text = "Auger Rate Avg";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_augAvg
            // 
            this.dy_augAvg.AutoSize = true;
            this.dy_augAvg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_augAvg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_augAvg.Location = new System.Drawing.Point(202, 120);
            this.dy_augAvg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_augAvg.Name = "dy_augAvg";
            this.dy_augAvg.Size = new System.Drawing.Size(156, 30);
            this.dy_augAvg.TabIndex = 74;
            this.dy_augAvg.Text = "{val}";
            this.dy_augAvg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(362, 0);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(196, 30);
            this.label14.TabIndex = 71;
            this.label14.Text = "Auger CL";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(2, 150);
            this.label28.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(196, 30);
            this.label28.TabIndex = 94;
            this.label28.Text = "Distance*";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(2, 210);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(196, 30);
            this.label29.TabIndex = 95;
            this.label29.Text = "Miles Blasted*";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_augerCl
            // 
            this.dy_augerCl.AutoSize = true;
            this.dy_augerCl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_augerCl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_augerCl.Location = new System.Drawing.Point(562, 0);
            this.dy_augerCl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_augerCl.Name = "dy_augerCl";
            this.dy_augerCl.Size = new System.Drawing.Size(156, 30);
            this.dy_augerCl.TabIndex = 72;
            this.dy_augerCl.Text = "{val}";
            this.dy_augerCl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_miles
            // 
            this.dy_miles.AutoSize = true;
            this.dy_miles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_miles.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_miles.Location = new System.Drawing.Point(202, 150);
            this.dy_miles.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_miles.Name = "dy_miles";
            this.dy_miles.Size = new System.Drawing.Size(156, 30);
            this.dy_miles.TabIndex = 101;
            this.dy_miles.Text = "{val}";
            this.dy_miles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_milesB
            // 
            this.dy_milesB.AutoSize = true;
            this.dy_milesB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_milesB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_milesB.Location = new System.Drawing.Point(202, 210);
            this.dy_milesB.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_milesB.Name = "dy_milesB";
            this.dy_milesB.Size = new System.Drawing.Size(156, 30);
            this.dy_milesB.TabIndex = 102;
            this.dy_milesB.Text = "{val}";
            this.dy_milesB.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_prodN
            // 
            this.dy_prodN.AutoSize = true;
            this.dy_prodN.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_prodN.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_prodN.Location = new System.Drawing.Point(562, 180);
            this.dy_prodN.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_prodN.Name = "dy_prodN";
            this.dy_prodN.Size = new System.Drawing.Size(156, 30);
            this.dy_prodN.TabIndex = 105;
            this.dy_prodN.Text = "{val}";
            this.dy_prodN.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_blast
            // 
            this.dy_blast.AutoSize = true;
            this.dy_blast.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_blast.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_blast.Location = new System.Drawing.Point(562, 120);
            this.dy_blast.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_blast.Name = "dy_blast";
            this.dy_blast.Size = new System.Drawing.Size(156, 30);
            this.dy_blast.TabIndex = 100;
            this.dy_blast.Text = "{val}";
            this.dy_blast.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_pause
            // 
            this.dy_pause.AutoSize = true;
            this.dy_pause.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_pause.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_pause.Location = new System.Drawing.Point(562, 90);
            this.dy_pause.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_pause.Name = "dy_pause";
            this.dy_pause.Size = new System.Drawing.Size(156, 30);
            this.dy_pause.TabIndex = 99;
            this.dy_pause.Text = "{val}";
            this.dy_pause.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(2, 240);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(196, 30);
            this.label31.TabIndex = 81;
            this.label31.Text = "Blast Output*";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_blastO
            // 
            this.dy_blastO.AutoSize = true;
            this.dy_blastO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_blastO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_blastO.Location = new System.Drawing.Point(202, 240);
            this.dy_blastO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_blastO.Name = "dy_blastO";
            this.dy_blastO.Size = new System.Drawing.Size(156, 30);
            this.dy_blastO.TabIndex = 82;
            this.dy_blastO.Text = "{val}";
            this.dy_blastO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(362, 30);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(196, 30);
            this.label13.TabIndex = 106;
            this.label13.Text = "Auto Mode";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_autoMode
            // 
            this.dy_autoMode.AutoSize = true;
            this.dy_autoMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_autoMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_autoMode.Location = new System.Drawing.Point(562, 30);
            this.dy_autoMode.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_autoMode.Name = "dy_autoMode";
            this.dy_autoMode.Size = new System.Drawing.Size(156, 30);
            this.dy_autoMode.TabIndex = 107;
            this.dy_autoMode.Text = "{val}";
            this.dy_autoMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(362, 120);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(196, 30);
            this.label17.TabIndex = 93;
            this.label17.Text = "Blasting";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(362, 90);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(196, 30);
            this.label16.TabIndex = 92;
            this.label16.Text = "Paused";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(362, 180);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(196, 30);
            this.label38.TabIndex = 98;
            this.label38.Text = "Prod Name";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(2, 270);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(196, 30);
            this.label30.TabIndex = 96;
            this.label30.Text = "Product Output*";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(2, 300);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(196, 30);
            this.label35.TabIndex = 97;
            this.label35.Text = "Total Product";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_prodO
            // 
            this.dy_prodO.AutoSize = true;
            this.dy_prodO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_prodO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_prodO.Location = new System.Drawing.Point(202, 270);
            this.dy_prodO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_prodO.Name = "dy_prodO";
            this.dy_prodO.Size = new System.Drawing.Size(156, 30);
            this.dy_prodO.TabIndex = 103;
            this.dy_prodO.Text = "{val}";
            this.dy_prodO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_prodT
            // 
            this.dy_prodT.AutoSize = true;
            this.dy_prodT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_prodT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_prodT.Location = new System.Drawing.Point(202, 300);
            this.dy_prodT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_prodT.Name = "dy_prodT";
            this.dy_prodT.Size = new System.Drawing.Size(156, 30);
            this.dy_prodT.TabIndex = 104;
            this.dy_prodT.Text = "{val}";
            this.dy_prodT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(362, 240);
            this.label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(196, 30);
            this.label43.TabIndex = 108;
            this.label43.Text = "Body ID";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_bodyID
            // 
            this.dy_bodyID.AutoSize = true;
            this.dy_bodyID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_bodyID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_bodyID.Location = new System.Drawing.Point(562, 240);
            this.dy_bodyID.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_bodyID.Name = "dy_bodyID";
            this.dy_bodyID.Size = new System.Drawing.Size(156, 30);
            this.dy_bodyID.TabIndex = 109;
            this.dy_bodyID.Text = "{val}";
            this.dy_bodyID.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(362, 150);
            this.label47.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(196, 30);
            this.label47.TabIndex = 110;
            this.label47.Text = "Lane Mode";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_laneMode
            // 
            this.dy_laneMode.AutoSize = true;
            this.dy_laneMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_laneMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_laneMode.Location = new System.Drawing.Point(562, 150);
            this.dy_laneMode.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_laneMode.Name = "dy_laneMode";
            this.dy_laneMode.Size = new System.Drawing.Size(156, 30);
            this.dy_laneMode.TabIndex = 111;
            this.dy_laneMode.Text = "{val}";
            this.dy_laneMode.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(912, 526);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Pre-Wet";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.dy_tankFlush, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.label105, 2, 4);
            this.tableLayoutPanel3.Controls.Add(this.dy_liqName, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.label88, 2, 3);
            this.tableLayoutPanel3.Controls.Add(this.label19, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label15, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label23, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label24, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label26, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label27, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label32, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label36, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.dy_liqT, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.dy_aiR, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.dy_pwR, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.dy_pwAvg, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.dy_aiO, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.dy_pwO, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.dy_aiAvg, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.dy_pwM, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.dy_pwCl, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label41, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.dy_liquidTankLvl, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.dy_lowLiq, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.label49, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.label91, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.label92, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.label93, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.label94, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.label95, 0, 13);
            this.tableLayoutPanel3.Controls.Add(this.label96, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.label100, 2, 10);
            this.tableLayoutPanel3.Controls.Add(this.label101, 2, 11);
            this.tableLayoutPanel3.Controls.Add(this.label102, 2, 12);
            this.tableLayoutPanel3.Controls.Add(this.label103, 2, 13);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom1, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom2, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom3, 1, 11);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom4, 1, 12);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom5, 1, 13);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom6, 3, 9);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom7, 3, 10);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom8, 3, 11);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom9, 3, 12);
            this.tableLayoutPanel3.Controls.Add(this.dy_boom10, 3, 13);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 15;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(912, 526);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // dy_liqName
            // 
            this.dy_liqName.AutoSize = true;
            this.dy_liqName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_liqName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_liqName.Location = new System.Drawing.Point(562, 90);
            this.dy_liqName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_liqName.Name = "dy_liqName";
            this.dy_liqName.Size = new System.Drawing.Size(156, 30);
            this.dy_liqName.TabIndex = 113;
            this.dy_liqName.Text = "{val}";
            this.dy_liqName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(362, 90);
            this.label88.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(196, 30);
            this.label88.TabIndex = 112;
            this.label88.Text = "Liquid Product";
            this.label88.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(2, 0);
            this.label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(196, 30);
            this.label19.TabIndex = 88;
            this.label19.Text = "Pre-Wet Mode";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(362, 0);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(196, 30);
            this.label15.TabIndex = 87;
            this.label15.Text = "Pre-Wet CL";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(2, 30);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(196, 30);
            this.label23.TabIndex = 89;
            this.label23.Text = "Pre-Wet Rate";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(2, 60);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(196, 30);
            this.label24.TabIndex = 90;
            this.label24.Text = "Anti-Ice Rate";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(2, 90);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(196, 30);
            this.label26.TabIndex = 91;
            this.label26.Text = "Pre-Wet Rate Avg";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(2, 120);
            this.label27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(196, 30);
            this.label27.TabIndex = 92;
            this.label27.Text = "Anti-Ice Rate Avg";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(2, 150);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(196, 30);
            this.label33.TabIndex = 94;
            this.label33.Text = "Anti-Ice Output*";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(2, 180);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(196, 30);
            this.label32.TabIndex = 93;
            this.label32.Text = "Pre-Wet Output*";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(2, 210);
            this.label36.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(196, 30);
            this.label36.TabIndex = 95;
            this.label36.Text = "Total Liquid";
            this.label36.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_liqT
            // 
            this.dy_liqT.AutoSize = true;
            this.dy_liqT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_liqT.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_liqT.Location = new System.Drawing.Point(202, 210);
            this.dy_liqT.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_liqT.Name = "dy_liqT";
            this.dy_liqT.Size = new System.Drawing.Size(156, 30);
            this.dy_liqT.TabIndex = 104;
            this.dy_liqT.Text = "{val}";
            this.dy_liqT.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_aiR
            // 
            this.dy_aiR.AutoSize = true;
            this.dy_aiR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_aiR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_aiR.Location = new System.Drawing.Point(202, 60);
            this.dy_aiR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_aiR.Name = "dy_aiR";
            this.dy_aiR.Size = new System.Drawing.Size(156, 30);
            this.dy_aiR.TabIndex = 99;
            this.dy_aiR.Text = "{val}";
            this.dy_aiR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_pwR
            // 
            this.dy_pwR.AutoSize = true;
            this.dy_pwR.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_pwR.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_pwR.Location = new System.Drawing.Point(202, 30);
            this.dy_pwR.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_pwR.Name = "dy_pwR";
            this.dy_pwR.Size = new System.Drawing.Size(156, 30);
            this.dy_pwR.TabIndex = 98;
            this.dy_pwR.Text = "{val}";
            this.dy_pwR.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_pwAvg
            // 
            this.dy_pwAvg.AutoSize = true;
            this.dy_pwAvg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_pwAvg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_pwAvg.Location = new System.Drawing.Point(202, 90);
            this.dy_pwAvg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_pwAvg.Name = "dy_pwAvg";
            this.dy_pwAvg.Size = new System.Drawing.Size(156, 30);
            this.dy_pwAvg.TabIndex = 100;
            this.dy_pwAvg.Text = "{val}";
            this.dy_pwAvg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_aiO
            // 
            this.dy_aiO.AutoSize = true;
            this.dy_aiO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_aiO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_aiO.Location = new System.Drawing.Point(202, 150);
            this.dy_aiO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_aiO.Name = "dy_aiO";
            this.dy_aiO.Size = new System.Drawing.Size(156, 30);
            this.dy_aiO.TabIndex = 103;
            this.dy_aiO.Text = "{val}";
            this.dy_aiO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_pwO
            // 
            this.dy_pwO.AutoSize = true;
            this.dy_pwO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_pwO.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_pwO.Location = new System.Drawing.Point(202, 180);
            this.dy_pwO.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_pwO.Name = "dy_pwO";
            this.dy_pwO.Size = new System.Drawing.Size(156, 30);
            this.dy_pwO.TabIndex = 102;
            this.dy_pwO.Text = "{val}";
            this.dy_pwO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_aiAvg
            // 
            this.dy_aiAvg.AutoSize = true;
            this.dy_aiAvg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_aiAvg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_aiAvg.Location = new System.Drawing.Point(202, 120);
            this.dy_aiAvg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_aiAvg.Name = "dy_aiAvg";
            this.dy_aiAvg.Size = new System.Drawing.Size(156, 30);
            this.dy_aiAvg.TabIndex = 101;
            this.dy_aiAvg.Text = "{val}";
            this.dy_aiAvg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_pwM
            // 
            this.dy_pwM.AutoSize = true;
            this.dy_pwM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_pwM.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_pwM.Location = new System.Drawing.Point(202, 0);
            this.dy_pwM.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_pwM.Name = "dy_pwM";
            this.dy_pwM.Size = new System.Drawing.Size(156, 30);
            this.dy_pwM.TabIndex = 97;
            this.dy_pwM.Text = "{val}";
            this.dy_pwM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_pwCl
            // 
            this.dy_pwCl.AutoSize = true;
            this.dy_pwCl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_pwCl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_pwCl.Location = new System.Drawing.Point(562, 0);
            this.dy_pwCl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_pwCl.Name = "dy_pwCl";
            this.dy_pwCl.Size = new System.Drawing.Size(156, 30);
            this.dy_pwCl.TabIndex = 96;
            this.dy_pwCl.Text = "{val}";
            this.dy_pwCl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(362, 30);
            this.label41.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(196, 30);
            this.label41.TabIndex = 105;
            this.label41.Text = "Liquid Tank Level";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_liquidTankLvl
            // 
            this.dy_liquidTankLvl.AutoSize = true;
            this.dy_liquidTankLvl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_liquidTankLvl.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_liquidTankLvl.Location = new System.Drawing.Point(562, 30);
            this.dy_liquidTankLvl.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_liquidTankLvl.Name = "dy_liquidTankLvl";
            this.dy_liquidTankLvl.Size = new System.Drawing.Size(156, 30);
            this.dy_liquidTankLvl.TabIndex = 106;
            this.dy_liquidTankLvl.Text = "{val}";
            this.dy_liquidTankLvl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_lowLiq
            // 
            this.dy_lowLiq.AutoSize = true;
            this.dy_lowLiq.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_lowLiq.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_lowLiq.Location = new System.Drawing.Point(562, 60);
            this.dy_lowLiq.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_lowLiq.Name = "dy_lowLiq";
            this.dy_lowLiq.Size = new System.Drawing.Size(156, 30);
            this.dy_lowLiq.TabIndex = 110;
            this.dy_lowLiq.Text = "{val}";
            this.dy_lowLiq.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(362, 60);
            this.label49.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(196, 30);
            this.label49.TabIndex = 111;
            this.label49.Text = "Low Liquid Flag";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(2, 270);
            this.label91.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(196, 30);
            this.label91.TabIndex = 114;
            this.label91.Text = "Boom #1";
            this.label91.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(2, 300);
            this.label92.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(196, 30);
            this.label92.TabIndex = 115;
            this.label92.Text = "Boom #2";
            this.label92.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(2, 330);
            this.label93.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(196, 30);
            this.label93.TabIndex = 116;
            this.label93.Text = "Boom #3";
            this.label93.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(2, 360);
            this.label94.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(196, 30);
            this.label94.TabIndex = 117;
            this.label94.Text = "Boom #4";
            this.label94.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(2, 390);
            this.label95.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(196, 30);
            this.label95.TabIndex = 118;
            this.label95.Text = "Boom #5";
            this.label95.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(362, 270);
            this.label96.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(196, 30);
            this.label96.TabIndex = 119;
            this.label96.Text = "Boom #6";
            this.label96.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(362, 300);
            this.label100.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(196, 30);
            this.label100.TabIndex = 120;
            this.label100.Text = "Boom #7";
            this.label100.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(362, 330);
            this.label101.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(196, 30);
            this.label101.TabIndex = 121;
            this.label101.Text = "Boom #8";
            this.label101.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(362, 360);
            this.label102.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(196, 30);
            this.label102.TabIndex = 122;
            this.label102.Text = "Boom #9";
            this.label102.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(362, 390);
            this.label103.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(196, 30);
            this.label103.TabIndex = 123;
            this.label103.Text = "Boom #10";
            this.label103.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_boom1
            // 
            this.dy_boom1.AutoSize = true;
            this.dy_boom1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom1.Location = new System.Drawing.Point(202, 270);
            this.dy_boom1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom1.Name = "dy_boom1";
            this.dy_boom1.Size = new System.Drawing.Size(156, 30);
            this.dy_boom1.TabIndex = 124;
            this.dy_boom1.Text = "{val}";
            this.dy_boom1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom2
            // 
            this.dy_boom2.AutoSize = true;
            this.dy_boom2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom2.Location = new System.Drawing.Point(202, 300);
            this.dy_boom2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom2.Name = "dy_boom2";
            this.dy_boom2.Size = new System.Drawing.Size(156, 30);
            this.dy_boom2.TabIndex = 125;
            this.dy_boom2.Text = "{val}";
            this.dy_boom2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom3
            // 
            this.dy_boom3.AutoSize = true;
            this.dy_boom3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom3.Location = new System.Drawing.Point(202, 330);
            this.dy_boom3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom3.Name = "dy_boom3";
            this.dy_boom3.Size = new System.Drawing.Size(156, 30);
            this.dy_boom3.TabIndex = 126;
            this.dy_boom3.Text = "{val}";
            this.dy_boom3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom4
            // 
            this.dy_boom4.AutoSize = true;
            this.dy_boom4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom4.Location = new System.Drawing.Point(202, 360);
            this.dy_boom4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom4.Name = "dy_boom4";
            this.dy_boom4.Size = new System.Drawing.Size(156, 30);
            this.dy_boom4.TabIndex = 127;
            this.dy_boom4.Text = "{val}";
            this.dy_boom4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom5
            // 
            this.dy_boom5.AutoSize = true;
            this.dy_boom5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom5.Location = new System.Drawing.Point(202, 390);
            this.dy_boom5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom5.Name = "dy_boom5";
            this.dy_boom5.Size = new System.Drawing.Size(156, 30);
            this.dy_boom5.TabIndex = 128;
            this.dy_boom5.Text = "{val}";
            this.dy_boom5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom6
            // 
            this.dy_boom6.AutoSize = true;
            this.dy_boom6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom6.Location = new System.Drawing.Point(562, 270);
            this.dy_boom6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom6.Name = "dy_boom6";
            this.dy_boom6.Size = new System.Drawing.Size(156, 30);
            this.dy_boom6.TabIndex = 129;
            this.dy_boom6.Text = "{val}";
            this.dy_boom6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom7
            // 
            this.dy_boom7.AutoSize = true;
            this.dy_boom7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom7.Location = new System.Drawing.Point(562, 300);
            this.dy_boom7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom7.Name = "dy_boom7";
            this.dy_boom7.Size = new System.Drawing.Size(156, 30);
            this.dy_boom7.TabIndex = 130;
            this.dy_boom7.Text = "{val}";
            this.dy_boom7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom8
            // 
            this.dy_boom8.AutoSize = true;
            this.dy_boom8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom8.Location = new System.Drawing.Point(562, 330);
            this.dy_boom8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom8.Name = "dy_boom8";
            this.dy_boom8.Size = new System.Drawing.Size(156, 30);
            this.dy_boom8.TabIndex = 131;
            this.dy_boom8.Text = "{val}";
            this.dy_boom8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom9
            // 
            this.dy_boom9.AutoSize = true;
            this.dy_boom9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom9.Location = new System.Drawing.Point(562, 360);
            this.dy_boom9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom9.Name = "dy_boom9";
            this.dy_boom9.Size = new System.Drawing.Size(156, 30);
            this.dy_boom9.TabIndex = 132;
            this.dy_boom9.Text = "{val}";
            this.dy_boom9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_boom10
            // 
            this.dy_boom10.AutoSize = true;
            this.dy_boom10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_boom10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_boom10.Location = new System.Drawing.Point(562, 390);
            this.dy_boom10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_boom10.Name = "dy_boom10";
            this.dy_boom10.Size = new System.Drawing.Size(156, 30);
            this.dy_boom10.TabIndex = 133;
            this.dy_boom10.Text = "{val}";
            this.dy_boom10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tableLayoutPanel4);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(912, 526);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Digital Inputs";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 5;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.label48, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label50, 0, 12);
            this.tableLayoutPanel4.Controls.Add(this.label51, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label52, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label53, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.label54, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.label55, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.label56, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig3, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig2, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig4, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig6, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig7, 1, 6);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig5, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig13, 1, 12);
            this.tableLayoutPanel4.Controls.Add(this.label67, 0, 13);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig14, 1, 13);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig15, 1, 14);
            this.tableLayoutPanel4.Controls.Add(this.label70, 0, 14);
            this.tableLayoutPanel4.Controls.Add(this.label57, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.label72, 0, 8);
            this.tableLayoutPanel4.Controls.Add(this.label73, 0, 9);
            this.tableLayoutPanel4.Controls.Add(this.label71, 0, 10);
            this.tableLayoutPanel4.Controls.Add(this.label58, 0, 11);
            this.tableLayoutPanel4.Controls.Add(this.label76, 0, 15);
            this.tableLayoutPanel4.Controls.Add(this.label78, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.label77, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.label75, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.label74, 2, 3);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig8, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig9, 1, 8);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig10, 1, 9);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig11, 1, 10);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig12, 1, 11);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig16, 1, 15);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig17, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig18, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig19, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig20, 3, 3);
            this.tableLayoutPanel4.Controls.Add(this.label62, 2, 4);
            this.tableLayoutPanel4.Controls.Add(this.label63, 2, 5);
            this.tableLayoutPanel4.Controls.Add(this.label64, 2, 6);
            this.tableLayoutPanel4.Controls.Add(this.label66, 2, 7);
            this.tableLayoutPanel4.Controls.Add(this.label69, 2, 8);
            this.tableLayoutPanel4.Controls.Add(this.label80, 2, 9);
            this.tableLayoutPanel4.Controls.Add(this.label90, 2, 10);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig21, 3, 4);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig22, 3, 5);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig23, 3, 6);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig24, 3, 7);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig25, 3, 8);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig26, 3, 9);
            this.tableLayoutPanel4.Controls.Add(this.dy_dig27, 3, 10);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 17;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(912, 526);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(2, 0);
            this.label48.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(196, 30);
            this.label48.TabIndex = 88;
            this.label48.Text = "Primary Dig 1";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(2, 360);
            this.label50.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(196, 30);
            this.label50.TabIndex = 87;
            this.label50.Text = "MF #2 Dig 1";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(2, 30);
            this.label51.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(196, 30);
            this.label51.TabIndex = 89;
            this.label51.Text = "Primary Dig 2";
            this.label51.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(2, 60);
            this.label52.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(196, 30);
            this.label52.TabIndex = 90;
            this.label52.Text = "Primary Dig 3";
            this.label52.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(2, 90);
            this.label53.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(196, 30);
            this.label53.TabIndex = 91;
            this.label53.Text = "Primary Dig 4";
            this.label53.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(2, 120);
            this.label54.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(196, 30);
            this.label54.TabIndex = 92;
            this.label54.Text = "Motor Dig 1";
            this.label54.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(2, 150);
            this.label55.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(196, 30);
            this.label55.TabIndex = 94;
            this.label55.Text = "Motor Dig 2";
            this.label55.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(2, 180);
            this.label56.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(196, 30);
            this.label56.TabIndex = 93;
            this.label56.Text = "Motor Dig 3";
            this.label56.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_dig3
            // 
            this.dy_dig3.AutoSize = true;
            this.dy_dig3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig3.Location = new System.Drawing.Point(202, 60);
            this.dy_dig3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig3.Name = "dy_dig3";
            this.dy_dig3.Size = new System.Drawing.Size(156, 30);
            this.dy_dig3.TabIndex = 99;
            this.dy_dig3.Text = "{val}";
            this.dy_dig3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig2
            // 
            this.dy_dig2.AutoSize = true;
            this.dy_dig2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig2.Location = new System.Drawing.Point(202, 30);
            this.dy_dig2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig2.Name = "dy_dig2";
            this.dy_dig2.Size = new System.Drawing.Size(156, 30);
            this.dy_dig2.TabIndex = 98;
            this.dy_dig2.Text = "{val}";
            this.dy_dig2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig4
            // 
            this.dy_dig4.AutoSize = true;
            this.dy_dig4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig4.Location = new System.Drawing.Point(202, 90);
            this.dy_dig4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig4.Name = "dy_dig4";
            this.dy_dig4.Size = new System.Drawing.Size(156, 30);
            this.dy_dig4.TabIndex = 100;
            this.dy_dig4.Text = "{val}";
            this.dy_dig4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig6
            // 
            this.dy_dig6.AutoSize = true;
            this.dy_dig6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig6.Location = new System.Drawing.Point(202, 150);
            this.dy_dig6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig6.Name = "dy_dig6";
            this.dy_dig6.Size = new System.Drawing.Size(156, 30);
            this.dy_dig6.TabIndex = 103;
            this.dy_dig6.Text = "{val}";
            this.dy_dig6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig7
            // 
            this.dy_dig7.AutoSize = true;
            this.dy_dig7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig7.Location = new System.Drawing.Point(202, 180);
            this.dy_dig7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig7.Name = "dy_dig7";
            this.dy_dig7.Size = new System.Drawing.Size(156, 30);
            this.dy_dig7.TabIndex = 102;
            this.dy_dig7.Text = "{val}";
            this.dy_dig7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig5
            // 
            this.dy_dig5.AutoSize = true;
            this.dy_dig5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig5.Location = new System.Drawing.Point(202, 120);
            this.dy_dig5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig5.Name = "dy_dig5";
            this.dy_dig5.Size = new System.Drawing.Size(156, 30);
            this.dy_dig5.TabIndex = 101;
            this.dy_dig5.Text = "{val}";
            this.dy_dig5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig1
            // 
            this.dy_dig1.AutoSize = true;
            this.dy_dig1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig1.Location = new System.Drawing.Point(202, 0);
            this.dy_dig1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig1.Name = "dy_dig1";
            this.dy_dig1.Size = new System.Drawing.Size(156, 30);
            this.dy_dig1.TabIndex = 97;
            this.dy_dig1.Text = "{val}";
            this.dy_dig1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig13
            // 
            this.dy_dig13.AutoSize = true;
            this.dy_dig13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig13.Location = new System.Drawing.Point(202, 360);
            this.dy_dig13.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig13.Name = "dy_dig13";
            this.dy_dig13.Size = new System.Drawing.Size(156, 30);
            this.dy_dig13.TabIndex = 96;
            this.dy_dig13.Text = "{val}";
            this.dy_dig13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(2, 390);
            this.label67.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(196, 30);
            this.label67.TabIndex = 105;
            this.label67.Text = "MF #2 Dig 2";
            this.label67.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_dig14
            // 
            this.dy_dig14.AutoSize = true;
            this.dy_dig14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig14.Location = new System.Drawing.Point(202, 390);
            this.dy_dig14.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig14.Name = "dy_dig14";
            this.dy_dig14.Size = new System.Drawing.Size(156, 30);
            this.dy_dig14.TabIndex = 106;
            this.dy_dig14.Text = "{val}";
            this.dy_dig14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig15
            // 
            this.dy_dig15.AutoSize = true;
            this.dy_dig15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig15.Location = new System.Drawing.Point(202, 420);
            this.dy_dig15.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig15.Name = "dy_dig15";
            this.dy_dig15.Size = new System.Drawing.Size(156, 30);
            this.dy_dig15.TabIndex = 110;
            this.dy_dig15.Text = "{val}";
            this.dy_dig15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(2, 420);
            this.label70.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(196, 30);
            this.label70.TabIndex = 111;
            this.label70.Text = "MF #2 Dig 3";
            this.label70.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(2, 210);
            this.label57.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(196, 30);
            this.label57.TabIndex = 112;
            this.label57.Text = "Motor Dig 4";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(2, 240);
            this.label72.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(196, 30);
            this.label72.TabIndex = 115;
            this.label72.Text = "MF #1 Dig 1";
            this.label72.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(2, 270);
            this.label73.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(196, 30);
            this.label73.TabIndex = 116;
            this.label73.Text = "MF #1 Dig 2";
            this.label73.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(2, 300);
            this.label71.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(196, 30);
            this.label71.TabIndex = 114;
            this.label71.Text = "MF #1 Dig 3";
            this.label71.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(2, 330);
            this.label58.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(196, 30);
            this.label58.TabIndex = 113;
            this.label58.Text = "MF #1 Dig 4";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(2, 450);
            this.label76.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(196, 30);
            this.label76.TabIndex = 121;
            this.label76.Text = "MF #2 Dig 4";
            this.label76.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(362, 0);
            this.label78.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(196, 30);
            this.label78.TabIndex = 118;
            this.label78.Text = "MF #3 Dig 1";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(362, 30);
            this.label77.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(196, 30);
            this.label77.TabIndex = 119;
            this.label77.Text = "MF #3 Dig 2";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(362, 60);
            this.label75.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(196, 30);
            this.label75.TabIndex = 120;
            this.label75.Text = "MF #3 Dig 3";
            this.label75.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(362, 90);
            this.label74.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(196, 30);
            this.label74.TabIndex = 117;
            this.label74.Text = "MF #3 Dig 4";
            this.label74.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_dig8
            // 
            this.dy_dig8.AutoSize = true;
            this.dy_dig8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig8.Location = new System.Drawing.Point(202, 210);
            this.dy_dig8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig8.Name = "dy_dig8";
            this.dy_dig8.Size = new System.Drawing.Size(156, 30);
            this.dy_dig8.TabIndex = 122;
            this.dy_dig8.Text = "{val}";
            this.dy_dig8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig9
            // 
            this.dy_dig9.AutoSize = true;
            this.dy_dig9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig9.Location = new System.Drawing.Point(202, 240);
            this.dy_dig9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig9.Name = "dy_dig9";
            this.dy_dig9.Size = new System.Drawing.Size(156, 30);
            this.dy_dig9.TabIndex = 123;
            this.dy_dig9.Text = "{val}";
            this.dy_dig9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig10
            // 
            this.dy_dig10.AutoSize = true;
            this.dy_dig10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig10.Location = new System.Drawing.Point(202, 270);
            this.dy_dig10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig10.Name = "dy_dig10";
            this.dy_dig10.Size = new System.Drawing.Size(156, 30);
            this.dy_dig10.TabIndex = 124;
            this.dy_dig10.Text = "{val}";
            this.dy_dig10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig11
            // 
            this.dy_dig11.AutoSize = true;
            this.dy_dig11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig11.Location = new System.Drawing.Point(202, 300);
            this.dy_dig11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig11.Name = "dy_dig11";
            this.dy_dig11.Size = new System.Drawing.Size(156, 30);
            this.dy_dig11.TabIndex = 125;
            this.dy_dig11.Text = "{val}";
            this.dy_dig11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig12
            // 
            this.dy_dig12.AutoSize = true;
            this.dy_dig12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig12.Location = new System.Drawing.Point(202, 330);
            this.dy_dig12.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig12.Name = "dy_dig12";
            this.dy_dig12.Size = new System.Drawing.Size(156, 30);
            this.dy_dig12.TabIndex = 126;
            this.dy_dig12.Text = "{val}";
            this.dy_dig12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig16
            // 
            this.dy_dig16.AutoSize = true;
            this.dy_dig16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig16.Location = new System.Drawing.Point(202, 450);
            this.dy_dig16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig16.Name = "dy_dig16";
            this.dy_dig16.Size = new System.Drawing.Size(156, 30);
            this.dy_dig16.TabIndex = 128;
            this.dy_dig16.Text = "{val}";
            this.dy_dig16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig17
            // 
            this.dy_dig17.AutoSize = true;
            this.dy_dig17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig17.Location = new System.Drawing.Point(562, 0);
            this.dy_dig17.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig17.Name = "dy_dig17";
            this.dy_dig17.Size = new System.Drawing.Size(156, 30);
            this.dy_dig17.TabIndex = 127;
            this.dy_dig17.Text = "{val}";
            this.dy_dig17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig18
            // 
            this.dy_dig18.AutoSize = true;
            this.dy_dig18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig18.Location = new System.Drawing.Point(562, 30);
            this.dy_dig18.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig18.Name = "dy_dig18";
            this.dy_dig18.Size = new System.Drawing.Size(156, 30);
            this.dy_dig18.TabIndex = 129;
            this.dy_dig18.Text = "{val}";
            this.dy_dig18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig19
            // 
            this.dy_dig19.AutoSize = true;
            this.dy_dig19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig19.Location = new System.Drawing.Point(562, 60);
            this.dy_dig19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig19.Name = "dy_dig19";
            this.dy_dig19.Size = new System.Drawing.Size(156, 30);
            this.dy_dig19.TabIndex = 130;
            this.dy_dig19.Text = "{val}";
            this.dy_dig19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig20
            // 
            this.dy_dig20.AutoSize = true;
            this.dy_dig20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig20.Location = new System.Drawing.Point(562, 90);
            this.dy_dig20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig20.Name = "dy_dig20";
            this.dy_dig20.Size = new System.Drawing.Size(156, 30);
            this.dy_dig20.TabIndex = 131;
            this.dy_dig20.Text = "{val}";
            this.dy_dig20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(362, 120);
            this.label62.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(196, 30);
            this.label62.TabIndex = 132;
            this.label62.Text = "Sec #1 Dig";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(362, 150);
            this.label63.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(196, 30);
            this.label63.TabIndex = 133;
            this.label63.Text = "Sec #2 Dig";
            this.label63.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(362, 180);
            this.label64.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(196, 30);
            this.label64.TabIndex = 134;
            this.label64.Text = "Sec #3 Dig";
            this.label64.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(362, 210);
            this.label66.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(196, 30);
            this.label66.TabIndex = 135;
            this.label66.Text = "Sec #4 Dig";
            this.label66.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(362, 240);
            this.label69.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(196, 30);
            this.label69.TabIndex = 136;
            this.label69.Text = "Panel Dig 1";
            this.label69.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(362, 270);
            this.label80.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(196, 30);
            this.label80.TabIndex = 137;
            this.label80.Text = "Panel Dig 2";
            this.label80.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(362, 300);
            this.label90.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(196, 30);
            this.label90.TabIndex = 138;
            this.label90.Text = "Panel Dig 3";
            this.label90.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_dig21
            // 
            this.dy_dig21.AutoSize = true;
            this.dy_dig21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig21.Location = new System.Drawing.Point(562, 120);
            this.dy_dig21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig21.Name = "dy_dig21";
            this.dy_dig21.Size = new System.Drawing.Size(156, 30);
            this.dy_dig21.TabIndex = 140;
            this.dy_dig21.Text = "{val}";
            this.dy_dig21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig22
            // 
            this.dy_dig22.AutoSize = true;
            this.dy_dig22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig22.Location = new System.Drawing.Point(562, 150);
            this.dy_dig22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig22.Name = "dy_dig22";
            this.dy_dig22.Size = new System.Drawing.Size(156, 30);
            this.dy_dig22.TabIndex = 141;
            this.dy_dig22.Text = "{val}";
            this.dy_dig22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig23
            // 
            this.dy_dig23.AutoSize = true;
            this.dy_dig23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig23.Location = new System.Drawing.Point(562, 180);
            this.dy_dig23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig23.Name = "dy_dig23";
            this.dy_dig23.Size = new System.Drawing.Size(156, 30);
            this.dy_dig23.TabIndex = 142;
            this.dy_dig23.Text = "{val}";
            this.dy_dig23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig24
            // 
            this.dy_dig24.AutoSize = true;
            this.dy_dig24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig24.Location = new System.Drawing.Point(562, 210);
            this.dy_dig24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig24.Name = "dy_dig24";
            this.dy_dig24.Size = new System.Drawing.Size(156, 30);
            this.dy_dig24.TabIndex = 143;
            this.dy_dig24.Text = "{val}";
            this.dy_dig24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig25
            // 
            this.dy_dig25.AutoSize = true;
            this.dy_dig25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig25.Location = new System.Drawing.Point(562, 240);
            this.dy_dig25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig25.Name = "dy_dig25";
            this.dy_dig25.Size = new System.Drawing.Size(156, 30);
            this.dy_dig25.TabIndex = 144;
            this.dy_dig25.Text = "{val}";
            this.dy_dig25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig26
            // 
            this.dy_dig26.AutoSize = true;
            this.dy_dig26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig26.Location = new System.Drawing.Point(562, 270);
            this.dy_dig26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig26.Name = "dy_dig26";
            this.dy_dig26.Size = new System.Drawing.Size(156, 30);
            this.dy_dig26.TabIndex = 145;
            this.dy_dig26.Text = "{val}";
            this.dy_dig26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_dig27
            // 
            this.dy_dig27.AutoSize = true;
            this.dy_dig27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_dig27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_dig27.Location = new System.Drawing.Point(562, 300);
            this.dy_dig27.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_dig27.Name = "dy_dig27";
            this.dy_dig27.Size = new System.Drawing.Size(156, 30);
            this.dy_dig27.TabIndex = 146;
            this.dy_dig27.Text = "{val}";
            this.dy_dig27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tableLayoutPanel5);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(912, 526);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "ARTS Sensor";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 7;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.dy_arts_sen, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label89, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label61, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.dy_arts_ver, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.label65, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.dy_arts_serial, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.label68, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.dy_arts_obj, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.label79, 0, 5);
            this.tableLayoutPanel5.Controls.Add(this.dy_arts_air, 1, 5);
            this.tableLayoutPanel5.Controls.Add(this.label97, 0, 6);
            this.tableLayoutPanel5.Controls.Add(this.label98, 0, 7);
            this.tableLayoutPanel5.Controls.Add(this.label99, 0, 9);
            this.tableLayoutPanel5.Controls.Add(this.dy_arts_humidity, 1, 6);
            this.tableLayoutPanel5.Controls.Add(this.dy_arts_pressure, 1, 7);
            this.tableLayoutPanel5.Controls.Add(this.dy_arts_cfg, 1, 9);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 15;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(912, 526);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // dy_arts_sen
            // 
            this.dy_arts_sen.AutoSize = true;
            this.dy_arts_sen.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_arts_sen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_arts_sen.Location = new System.Drawing.Point(202, 0);
            this.dy_arts_sen.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_arts_sen.Name = "dy_arts_sen";
            this.dy_arts_sen.Size = new System.Drawing.Size(156, 26);
            this.dy_arts_sen.TabIndex = 108;
            this.dy_arts_sen.Text = "{val}";
            this.dy_arts_sen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(2, 0);
            this.label89.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(196, 26);
            this.label89.TabIndex = 107;
            this.label89.Text = "Sensor";
            this.label89.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(2, 52);
            this.label61.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(196, 26);
            this.label61.TabIndex = 0;
            this.label61.Text = "ARTS Version";
            this.label61.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_arts_ver
            // 
            this.dy_arts_ver.AutoSize = true;
            this.dy_arts_ver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_arts_ver.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_arts_ver.Location = new System.Drawing.Point(202, 52);
            this.dy_arts_ver.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_arts_ver.Name = "dy_arts_ver";
            this.dy_arts_ver.Size = new System.Drawing.Size(156, 26);
            this.dy_arts_ver.TabIndex = 40;
            this.dy_arts_ver.Text = "{val}";
            this.dy_arts_ver.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(2, 26);
            this.label65.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(196, 26);
            this.label65.TabIndex = 4;
            this.label65.Text = "Serial";
            this.label65.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_arts_serial
            // 
            this.dy_arts_serial.AutoSize = true;
            this.dy_arts_serial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_arts_serial.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_arts_serial.Location = new System.Drawing.Point(202, 26);
            this.dy_arts_serial.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_arts_serial.Name = "dy_arts_serial";
            this.dy_arts_serial.Size = new System.Drawing.Size(156, 26);
            this.dy_arts_serial.TabIndex = 41;
            this.dy_arts_serial.Text = "{val}";
            this.dy_arts_serial.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(2, 104);
            this.label68.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(196, 26);
            this.label68.TabIndex = 2;
            this.label68.Text = "Obj Temperature";
            this.label68.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_arts_obj
            // 
            this.dy_arts_obj.AutoSize = true;
            this.dy_arts_obj.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_arts_obj.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_arts_obj.Location = new System.Drawing.Point(202, 104);
            this.dy_arts_obj.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_arts_obj.Name = "dy_arts_obj";
            this.dy_arts_obj.Size = new System.Drawing.Size(156, 26);
            this.dy_arts_obj.TabIndex = 45;
            this.dy_arts_obj.Text = "{val}";
            this.dy_arts_obj.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(2, 130);
            this.label79.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(196, 26);
            this.label79.TabIndex = 1;
            this.label79.Text = "Air Temperature";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_arts_air
            // 
            this.dy_arts_air.AutoSize = true;
            this.dy_arts_air.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_arts_air.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_arts_air.Location = new System.Drawing.Point(202, 130);
            this.dy_arts_air.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_arts_air.Name = "dy_arts_air";
            this.dy_arts_air.Size = new System.Drawing.Size(156, 26);
            this.dy_arts_air.TabIndex = 42;
            this.dy_arts_air.Text = "{val}";
            this.dy_arts_air.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(2, 156);
            this.label97.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(196, 26);
            this.label97.TabIndex = 91;
            this.label97.Text = "Humidity";
            this.label97.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(2, 182);
            this.label98.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(196, 26);
            this.label98.TabIndex = 92;
            this.label98.Text = "Pressure";
            this.label98.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(2, 234);
            this.label99.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(196, 26);
            this.label99.TabIndex = 93;
            this.label99.Text = "Emissitivity";
            this.label99.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_arts_humidity
            // 
            this.dy_arts_humidity.AutoSize = true;
            this.dy_arts_humidity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_arts_humidity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_arts_humidity.Location = new System.Drawing.Point(202, 156);
            this.dy_arts_humidity.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_arts_humidity.Name = "dy_arts_humidity";
            this.dy_arts_humidity.Size = new System.Drawing.Size(156, 26);
            this.dy_arts_humidity.TabIndex = 94;
            this.dy_arts_humidity.Text = "{val}";
            this.dy_arts_humidity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_arts_pressure
            // 
            this.dy_arts_pressure.AutoSize = true;
            this.dy_arts_pressure.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_arts_pressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_arts_pressure.Location = new System.Drawing.Point(202, 182);
            this.dy_arts_pressure.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_arts_pressure.Name = "dy_arts_pressure";
            this.dy_arts_pressure.Size = new System.Drawing.Size(156, 26);
            this.dy_arts_pressure.TabIndex = 95;
            this.dy_arts_pressure.Text = "{val}";
            this.dy_arts_pressure.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dy_arts_cfg
            // 
            this.dy_arts_cfg.AutoSize = true;
            this.dy_arts_cfg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_arts_cfg.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_arts_cfg.Location = new System.Drawing.Point(202, 234);
            this.dy_arts_cfg.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_arts_cfg.Name = "dy_arts_cfg";
            this.dy_arts_cfg.Size = new System.Drawing.Size(156, 26);
            this.dy_arts_cfg.TabIndex = 96;
            this.dy_arts_cfg.Text = "{val}";
            this.dy_arts_cfg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(920, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.startCANToolStripMenuItem,
            this.stopCANToolStripMenuItem,
            this.startRS232,
            this.stopRS232,
            this.toolStripSeparator2,
            this.startRoadWatchToolStripMenuItem,
            this.stopRoadwatchToolStripMenuItem,
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // startCANToolStripMenuItem
            // 
            this.startCANToolStripMenuItem.Name = "startCANToolStripMenuItem";
            this.startCANToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.startCANToolStripMenuItem.Text = "Start CAN";
            this.startCANToolStripMenuItem.Click += new System.EventHandler(this.startCANToolStripMenuItem_Click);
            // 
            // stopCANToolStripMenuItem
            // 
            this.stopCANToolStripMenuItem.Name = "stopCANToolStripMenuItem";
            this.stopCANToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.stopCANToolStripMenuItem.Text = "Stop CAN";
            this.stopCANToolStripMenuItem.Click += new System.EventHandler(this.stopCANToolStripMenuItem_Click);
            // 
            // startRS232
            // 
            this.startRS232.Name = "startRS232";
            this.startRS232.Size = new System.Drawing.Size(162, 22);
            this.startRS232.Text = "Start RS-232";
            this.startRS232.Click += new System.EventHandler(this.startRS232_Click);
            // 
            // stopRS232
            // 
            this.stopRS232.Name = "stopRS232";
            this.stopRS232.Size = new System.Drawing.Size(162, 22);
            this.stopRS232.Text = "Stop RS-232";
            this.stopRS232.Click += new System.EventHandler(this.stopRS232_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(159, 6);
            // 
            // startRoadWatchToolStripMenuItem
            // 
            this.startRoadWatchToolStripMenuItem.Name = "startRoadWatchToolStripMenuItem";
            this.startRoadWatchToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.startRoadWatchToolStripMenuItem.Text = "Start RoadWatch";
            this.startRoadWatchToolStripMenuItem.Click += new System.EventHandler(this.startRoadWatchToolStripMenuItem_Click);
            // 
            // stopRoadwatchToolStripMenuItem
            // 
            this.stopRoadwatchToolStripMenuItem.Name = "stopRoadwatchToolStripMenuItem";
            this.stopRoadwatchToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.stopRoadwatchToolStripMenuItem.Text = "Stop Roadwatch";
            this.stopRoadwatchToolStripMenuItem.Click += new System.EventHandler(this.stopRoadwatchToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(159, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(162, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(362, 120);
            this.label105.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(196, 30);
            this.label105.TabIndex = 134;
            this.label105.Text = "Tank Flush";
            this.label105.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dy_tankFlush
            // 
            this.dy_tankFlush.AutoSize = true;
            this.dy_tankFlush.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dy_tankFlush.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dy_tankFlush.Location = new System.Drawing.Point(562, 120);
            this.dy_tankFlush.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.dy_tankFlush.Name = "dy_tankFlush";
            this.dy_tankFlush.Size = new System.Drawing.Size(156, 30);
            this.dy_tankFlush.TabIndex = 135;
            this.dy_tankFlush.Text = "{val}";
            this.dy_tankFlush.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 576);
            this.Controls.Add(this.tabPanel);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Advantage+ CAN Datalogging";
            this.tabPanel.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.rmt_rate1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rmt_rate2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rmt_rate3)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabPanel;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label dy_oilT;
        private System.Windows.Forms.Label dy_oillvl;
        private System.Windows.Forms.Label dy_loadPress;
        private System.Windows.Forms.Label dy_pumpPress;
        private System.Windows.Forms.Label dy_airTemp;
        private System.Windows.Forms.Label dy_roadTemp;
        private System.Windows.Forms.Label dy_voltage;
        private System.Windows.Forms.Label dy_gspeed;
        private System.Windows.Forms.Label dy_truckId;
        private System.Windows.Forms.Label dy_serial;
        private System.Windows.Forms.Label dy_lastCal;
        private System.Windows.Forms.Label dy_uiVer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label dy_ctrlVer;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label dy_auger2R;
        private System.Windows.Forms.Label dy_augerR;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label dy_spreaderM;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label dy_blastO;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label dy_spinR;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label dy_augerCl;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label dy_augAvg;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label dy_miles;
        private System.Windows.Forms.Label dy_milesB;
        private System.Windows.Forms.Label dy_prodN;
        private System.Windows.Forms.Label dy_blast;
        private System.Windows.Forms.Label dy_pause;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label dy_autoMode;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label dy_prodO;
        private System.Windows.Forms.Label dy_prodT;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label dy_liqT;
        private System.Windows.Forms.Label dy_aiR;
        private System.Windows.Forms.Label dy_pwR;
        private System.Windows.Forms.Label dy_pwAvg;
        private System.Windows.Forms.Label dy_aiO;
        private System.Windows.Forms.Label dy_pwO;
        private System.Windows.Forms.Label dy_aiAvg;
        private System.Windows.Forms.Label dy_pwM;
        private System.Windows.Forms.Label dy_pwCl;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label dy_runtime;
        private System.Windows.Forms.Label dy_route;
        private System.Windows.Forms.Label dy_driver;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label dy_liquidTankLvl;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopCANToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startCANToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.Label can_status;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label dy_bodyID;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label dy_laneMode;
        private System.Windows.Forms.Label dy_lowLiq;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.Label dy_dig3;
        private System.Windows.Forms.Label dy_dig2;
        private System.Windows.Forms.Label dy_dig4;
        private System.Windows.Forms.Label dy_dig6;
        private System.Windows.Forms.Label dy_dig7;
        private System.Windows.Forms.Label dy_dig5;
        private System.Windows.Forms.Label dy_dig1;
        private System.Windows.Forms.Label dy_dig13;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label dy_dig14;
        private System.Windows.Forms.Label dy_dig15;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label dy_dig8;
        private System.Windows.Forms.Label dy_dig9;
        private System.Windows.Forms.Label dy_dig10;
        private System.Windows.Forms.Label dy_dig11;
        private System.Windows.Forms.Label dy_dig12;
        private System.Windows.Forms.Label dy_dig16;
        private System.Windows.Forms.Label dy_dig17;
        private System.Windows.Forms.Label dy_dig18;
        private System.Windows.Forms.Label dy_dig19;
        private System.Windows.Forms.Label dy_dig20;
        private System.Windows.Forms.Label dy_coldOil;
        private System.Windows.Forms.Label dy_hotOil;
        private System.Windows.Forms.Label dy_lowOil;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label dy_arts_ver;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label dy_arts_serial;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label dy_arts_obj;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Label dy_arts_air;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label dy_arts_humidity;
        private System.Windows.Forms.Label dy_arts_pressure;
        private System.Windows.Forms.Label dy_arts_cfg;
        private System.Windows.Forms.ToolStripMenuItem startRoadWatchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopRoadwatchToolStripMenuItem;
        private System.Windows.Forms.Label j1708_status;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.Label dy_spreaderM2;
        private System.Windows.Forms.CheckBox rmt_rate3_on;
        private System.Windows.Forms.NumericUpDown rmt_rate1;
        private System.Windows.Forms.CheckBox rmt_rate1_on;
        private System.Windows.Forms.CheckBox rmt_rate2_on;
        private System.Windows.Forms.NumericUpDown rmt_rate2;
        private System.Windows.Forms.NumericUpDown rmt_rate3;
        private System.Windows.Forms.Label dy_protoVer;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.ToolStripMenuItem startRS232;
        private System.Windows.Forms.ToolStripMenuItem stopRS232;
        private System.Windows.Forms.Label rs232_status;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Label packets_rx;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Label dy_dist_sprd;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Label dy_liqName;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label dy_arts_sen;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label dy_dig21;
        private System.Windows.Forms.Label dy_dig22;
        private System.Windows.Forms.Label dy_dig23;
        private System.Windows.Forms.Label dy_dig24;
        private System.Windows.Forms.Label dy_dig25;
        private System.Windows.Forms.Label dy_dig26;
        private System.Windows.Forms.Label dy_dig27;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label dy_boom1;
        private System.Windows.Forms.Label dy_boom2;
        private System.Windows.Forms.Label dy_boom3;
        private System.Windows.Forms.Label dy_boom4;
        private System.Windows.Forms.Label dy_boom5;
        private System.Windows.Forms.Label dy_boom6;
        private System.Windows.Forms.Label dy_boom7;
        private System.Windows.Forms.Label dy_boom8;
        private System.Windows.Forms.Label dy_boom9;
        private System.Windows.Forms.Label dy_boom10;
        private System.Windows.Forms.Label dy_ts;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label dy_tankFlush;
        private System.Windows.Forms.Label label105;
    }
}

