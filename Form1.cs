﻿/*!
 * @file Form1.cs
 * @project Adv+ AVL Test Application
 *
 * @author Kevin Wijesekera <kwijesekera@munciepower.com>
 * @note Compiled with Visual Studio 2015
 * @copyright 2019 Muncie Power Products
 * @section DESCRIPTION
 * This file contains the UI for the system
 * 
 * @section LICENSE
 * 
 * Redistribution and use in source and binary forms, with or without modification, 
 * are permitted provided that the following conditions are met:
 * 
 * 1) Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 *   
 * 2) Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation and/or 
 *   other materials provided with the distribution.
 *   
 * 3) Neither the name of the copyright holder nor the names of its contributors 
 *   may be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY 
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT 
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, 
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED 
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
 * IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING 
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace ADV_CAN_Datalogging
{
    public partial class Form1 : Form
    {
        private SysBus busProcessor;

        private System.Windows.Forms.Timer timer;

        private Dictionary<int, string> body_types = new Dictionary<int, string>();

        public Form1()
        {
            InitializeComponent();
            busProcessor = new SysBus();

            timer = new System.Windows.Forms.Timer();
            timer.Tick += Timer_Tick;
            timer.Interval = 1000;
            timer.Start();

            body_types.Add(0, "No Spreader");
            body_types.Add(1, "Standard");
            body_types.Add(2, "Tow Plow (S)");
            body_types.Add(3, "Xzalt");
            body_types.Add(4, "Dist Spinner");
            body_types.Add(5, "FRS");
            body_types.Add(6, "Tow Plow (C)");
            body_types.Add(100, "Anti-Ice");
            body_types.Add(101, "Sprayer");
            body_types.Add(102, "Const Sprayer");
        }

        private void startCANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (busProcessor.StartCan())
            {
                can_status.Text = "Running";
                busProcessor.tx(0, null);
            }
        }

        private void stopCANToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (busProcessor.StopCan())
            {
                can_status.Text = "Off";
            }
        }

        private void startRoadWatchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (busProcessor.StartJ1708())
            {
                j1708_status.Text = "On";
            }
        }

        private void stopRoadwatchToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (busProcessor.StopJ1708())
            {
                j1708_status.Text = "Off";
            }
        }


        private void startRS232_Click(object sender, EventArgs e)
        {
            if (busProcessor.StartRS232())
            {
                rs232_status.Text = "On";
            }
        }

        private void stopRS232_Click(object sender, EventArgs e)
        {
            if (busProcessor.StopRS232())
            {
                rs232_status.Text = "Off";
            }
        }


        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void rmt_rate_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox button = sender as CheckBox;
            button.Text = (button.Checked) ? "On" : "Off";
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            if (busProcessor.IsRunning())
            {
                SysBusDataRx data = busProcessor.data();

                this.dy_ts.Text = String.Format("{0:G}", data.ts);
                this.dy_ctrlVer.Text = data.ctrlVer;
                this.dy_uiVer.Text = data.uiVer;
                this.dy_lastCal.Text = String.Format("{0:d}", data.lastCal);
                this.dy_serial.Text = data.serial;
                this.dy_truckId.Text = data.truckId;
                this.dy_runtime.Text = String.Format("{0}", data.systemRuntime);
                this.dy_protoVer.Text = String.Format("{0}", data.protocolVersion);
                this.dy_gspeed.Text = String.Format("{0} MPH", data.groundspeed);
                this.dy_voltage.Text = String.Format("{0:0.0} V", data.voltage);
                this.dy_airTemp.Text = (data.airTemp == 0xFFF) ? "No Sensor" : String.Format("{0:0.00} C", data.airTemp);
                this.dy_roadTemp.Text = (data.roadTemp == 0xFFF) ? "No Sensor" : String.Format("{0:0.00} C", data.roadTemp);
                this.dy_pumpPress.Text = String.Format("{0} PSI", data.pumpP);
                this.dy_loadPress.Text = String.Format("{0} PSI", data.loadP);
                this.dy_autoMode.Text = String.Format("{0:Auto;;Man}", data.auto.GetHashCode());
                this.dy_augerCl.Text = String.Format("{0:Closed;;Open}", data.augerCl.GetHashCode());
                this.dy_pwCl.Text = String.Format("{0:Closed;;Open}", data.prewetCl.GetHashCode());
                this.dy_pause.Text = String.Format("{0:Paused;;Running}", data.pause.GetHashCode());
                this.dy_blast.Text = String.Format("{0:Blasting;;Normal}", data.blast.GetHashCode());
                this.dy_laneMode.Text = String.Format("{0:Lane;;Distance}", data.laneMode.GetHashCode());
                if (body_types.ContainsKey(data.spreaderMode))
                {
                    this.dy_spreaderM.Text = body_types[data.spreaderMode];
                    this.dy_spreaderM2.Text = body_types[data.spreaderMode];
                }
                else
                {
                    this.dy_spreaderM.Text = "Err";
                    this.dy_spreaderM2.Text = "Err";
                }

                this.dy_augerR.Text = String.Format("{0}", data.augerRate);
                this.dy_auger2R.Text = String.Format("{0}", data.auger2Rate);
                this.dy_spinR.Text = String.Format("{0}", data.spinnerRate);
                this.dy_pwR.Text = String.Format("{0}", data.preWetRate);
                this.dy_aiR.Text = String.Format("{0}", data.antiIceRate);
                this.dy_augAvg.Text = String.Format("{0}", data.avgAuger);
                this.dy_pwAvg.Text = String.Format("{0}", data.avgPreWet);
                this.dy_aiAvg.Text = String.Format("{0}", data.avgAntiIce);

                //P2P
                if (data.protocolVersion == 0)
                {
                    this.dy_miles.Text = String.Format("{0:0.0000}", data.P2PDist);
                    this.dy_milesB.Text = String.Format("{0:0.0000}", data.P2PDistBlast);
                    this.dy_prodO.Text = String.Format("{0:0.0000}", data.P2PProdOutput);
                    this.dy_blastO.Text = String.Format("{0:0.0000}", data.P2PProdBlastOutput);
                    this.dy_pwO.Text = String.Format("{0:0.0000}", data.P2PPreWetOutput);
                    this.dy_aiO.Text = String.Format("{0:0.0000}", data.P2PAntiIceOutput);

                }
                //Extra Totals
                else
                {
                    this.dy_miles.Text = String.Format("{0}", data.totalDist);
                    this.dy_dist_sprd.Text = String.Format("{0}", data.totalDistSpread);
                    this.dy_milesB.Text = String.Format("{0}", data.totalDistBlast);
                    this.dy_blastO.Text = String.Format("{0}", data.totalProdBlast);
                    this.dy_prodO.Text = "";
                    this.dy_pwO.Text = "";
                    this.dy_aiO.Text = "";

                }
                this.dy_pwM.Text = String.Format("{0}", data.preWetMode);
                this.dy_oillvl.Text = (data.oilLevel == 0xFF) ? "No Sensor" : String.Format("{0} %", data.oilLevel);
                this.dy_prodT.Text = String.Format("{0}", data.totalProd);
                this.dy_oilT.Text = (data.oilTemp == 0xFFF) ? "No Sensor" : String.Format("{0:0.0} C", data.oilTemp);
                this.dy_liqT.Text = String.Format("{0}", data.totalLiq);
                this.dy_prodN.Text = data.prodName;
                this.dy_liqName.Text = data.liqName;
                this.dy_route.Text = data.route;
                this.dy_driver.Text = data.driver;
                this.dy_liquidTankLvl.Text = (data.liqTankLevel == 0xFF) ? "No Sensor" : String.Format("{0}%", data.liqTankLevel);

                this.dy_bodyID.Text = String.Format("{0}", data.bodyId);
                this.dy_dig1.Text = String.Format("{0:On;;Off}", data.dig[0].GetHashCode());
                this.dy_dig2.Text = String.Format("{0:On;;Off}", data.dig[1].GetHashCode());
                this.dy_dig3.Text = String.Format("{0:On;;Off}", data.dig[2].GetHashCode());
                this.dy_dig4.Text = String.Format("{0:On;;Off}", data.dig[3].GetHashCode());
                this.dy_dig5.Text = String.Format("{0:On;;Off}", data.dig[4].GetHashCode());
                this.dy_dig6.Text = String.Format("{0:On;;Off}", data.dig[5].GetHashCode());
                this.dy_dig7.Text = String.Format("{0:On;;Off}", data.dig[6].GetHashCode());
                this.dy_dig8.Text = String.Format("{0:On;;Off}", data.dig[7].GetHashCode());
                this.dy_dig9.Text = String.Format("{0:On;;Off}", data.dig[8].GetHashCode());
                this.dy_dig10.Text = String.Format("{0:On;;Off}", data.dig[9].GetHashCode());
                this.dy_dig11.Text = String.Format("{0:On;;Off}", data.dig[10].GetHashCode());
                this.dy_dig12.Text = String.Format("{0:On;;Off}", data.dig[11].GetHashCode());
                this.dy_dig13.Text = String.Format("{0:On;;Off}", data.dig[12].GetHashCode());
                this.dy_dig14.Text = String.Format("{0:On;;Off}", data.dig[13].GetHashCode());
                this.dy_dig15.Text = String.Format("{0:On;;Off}", data.dig[14].GetHashCode());
                this.dy_dig16.Text = String.Format("{0:On;;Off}", data.dig[15].GetHashCode());
                this.dy_dig17.Text = String.Format("{0:On;;Off}", data.dig[16].GetHashCode());
                this.dy_dig18.Text = String.Format("{0:On;;Off}", data.dig[17].GetHashCode());
                this.dy_dig19.Text = String.Format("{0:On;;Off}", data.dig[18].GetHashCode());
                this.dy_dig20.Text = String.Format("{0:On;;Off}", data.dig[19].GetHashCode());
                this.dy_dig21.Text = String.Format("{0:On;;Off}", data.dig[20].GetHashCode());
                this.dy_dig22.Text = String.Format("{0:On;;Off}", data.dig[21].GetHashCode());
                this.dy_dig23.Text = String.Format("{0:On;;Off}", data.dig[22].GetHashCode());
                this.dy_dig24.Text = String.Format("{0:On;;Off}", data.dig[23].GetHashCode());
                this.dy_dig25.Text = String.Format("{0:On;;Off}", data.dig[24].GetHashCode());
                this.dy_dig26.Text = String.Format("{0:On;;Off}", data.dig[25].GetHashCode());
                this.dy_dig27.Text = String.Format("{0:On;;Off}", data.dig[26].GetHashCode());
                this.dy_boom1.Text = String.Format("{0:On;;Off}", data.booms[0].GetHashCode());
                this.dy_boom2.Text = String.Format("{0:On;;Off}", data.booms[1].GetHashCode());
                this.dy_boom3.Text = String.Format("{0:On;;Off}", data.booms[2].GetHashCode());
                this.dy_boom4.Text = String.Format("{0:On;;Off}", data.booms[3].GetHashCode());
                this.dy_boom5.Text = String.Format("{0:On;;Off}", data.booms[4].GetHashCode());
                this.dy_boom6.Text = String.Format("{0:On;;Off}", data.booms[5].GetHashCode());
                this.dy_boom7.Text = String.Format("{0:On;;Off}", data.booms[6].GetHashCode());
                this.dy_boom8.Text = String.Format("{0:On;;Off}", data.booms[7].GetHashCode());
                this.dy_boom9.Text = String.Format("{0:On;;Off}", data.booms[8].GetHashCode());
                this.dy_boom10.Text = String.Format("{0:On;;Off}", data.booms[9].GetHashCode());
                this.dy_tankFlush.Text = String.Format("{0:Running;;Off}", data.tankFlush.GetHashCode());

                this.dy_hotOil.Text = String.Format("{0:On;;Off}", data.hotOil.GetHashCode());
                this.dy_coldOil.Text = String.Format("{0:On;;Off}", data.coldOil.GetHashCode());
                this.dy_lowOil.Text = String.Format("{0:On;;Off}", data.lowOil.GetHashCode());
                this.dy_lowLiq.Text = String.Format("{0:On;;Off}", data.lowLiquid.GetHashCode());
                this.dy_arts_serial.Text = data.senSerial;
                this.dy_arts_ver.Text = data.senVer;
                this.dy_arts_sen.Text = data.senType;
                this.dy_arts_air.Text = (data.senAirTemp == 0xFFFF) ? "No Sensor" : String.Format("{0:0.00} C", data.senAirTemp);
                this.dy_arts_obj.Text = (data.senRoadTemp == 0xFFFF) ? "No Sensor" : String.Format("{0:0.00} C", data.senRoadTemp);
                this.dy_arts_pressure.Text = (data.senPressure == 0xFFFF) ? "No Sensor" : String.Format("{0:0.00} dPa", data.senPressure);
                this.dy_arts_humidity.Text = (data.senHumidity == 0xFFFF) ? "No Sensor" : String.Format("{0:0.00} %", data.senHumidity);
                this.dy_arts_cfg.Text = String.Format("{0}", data.senCfg);

                this.packets_rx.Text = data.packetCounter.ToString();

                if (rmt_rate1_on.Checked || rmt_rate2_on.Checked || rmt_rate3_on.Checked)
                {
                    SysCanDataTx dataT = new SysCanDataTx();
                    dataT.ctrlRate[0] = (rmt_rate1_on.Checked) ? Convert.ToInt32(rmt_rate1.Value) : 0xFF;
                    dataT.ctrlRate[1] = (rmt_rate2_on.Checked) ? Convert.ToInt32(rmt_rate2.Value) : 0xFF;
                    dataT.ctrlRate[2] = (rmt_rate3_on.Checked) ? Convert.ToInt32(rmt_rate3.Value) : 0xFF;
                    busProcessor.tx(1, dataT);
                }
            }
        }
    }
}
